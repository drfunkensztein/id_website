<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'id_website_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '96z)> @R3I+nUnh=_jR^Un*<tZ_JXis+71ia[!J>x/(|%=@DeRk1v_U-<[j,V8SL');
define('SECURE_AUTH_KEY',  'qP)PfXW@v=w!PzBD|OjsZS>|_aJsT^H:y7lc.Q.k]#:u+pX]>Q#Ay@?H!0*s5U|{');
define('LOGGED_IN_KEY',    'g&JGqIR`I7|8PB^jb[veS+Pcz|y+w-ZlF%FBh9|BZd8e,[//2ETib,aMiP9QF^GH');
define('NONCE_KEY',        '&|}(`y*=E-,7~GIcY)Jd;p^ywj+`s(3}ri:`P)*g%Lr60Z;ylAKB3j G+B!Oe#QG');
define('AUTH_SALT',        '5GG%CaJE|E0|iK{9^dLWfi j%{G=/Wf*L6>--qX|C]3$%>Z>`+O~!#6*~QIU2>.v');
define('SECURE_AUTH_SALT', 'xo;}4|~y,:5~_9hJ4O[Qn|.%|Z;DY~U+4;)b 3Bp$lHPH{}o5s,|Z1Yxyg+N<A(M');
define('LOGGED_IN_SALT',   'QV? U{d!:#K.q;_ &9],QgvqKH<!LViY4&114G68FrE$4,^}{^t!~N 0=pqOxS6o');
define('NONCE_SALT',       '>jd89</ji-y]hZ>O&.!>%&b83B9l7p)-@G[CH+D3r.P]~DH24<8_YuZkcp(u[fv6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */


// Turns WordPress debugging on
define('WP_DEBUG', true);

// Tells WordPress to log everything to the /wp-content/debug.log file
define('WP_DEBUG_LOG', true);

// Doesn't force the PHP 'display_errors' variable to be on
define('WP_DEBUG_DISPLAY', false);

// Hides errors from being displayed on-screen
@ini_set('display_errors', 0);


define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


