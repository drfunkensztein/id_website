# Example Child theme for the Cornerstone starter theme

This is an example Child Theme for the Cornerstone starter theme. It includes a few basic CSS rules specific to WordPress to get you started. The Cornerstone parent theme purposely doesn't include any CSS in the CSS stylesheet so that you don't have to clutter your child theme having to reverse those CSS rules to design how you want.