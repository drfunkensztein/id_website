		<div class="row home-links pad-double">
			<div class="footer-box inverted medium-4 small-6 large-2 columns">
				<div class="address">
					<h5>call us </h5>
					<p>+61 3 9417 2205<br/>
						NZ Freecall:<br/>
						0800 955 481
					</p>
				</div>
			</div>
			<div class="footer-box medium-4 small-6 large-2 columns">
				<div>
					<h5>contact us </h5>
					<div class="vcard">
						<p class="adr">
							<span class="email"><a style="display:block; font-size:1.5em; color: #54514e; margin-bottom:-4px;" href="mailto:info@id.com.au">info@id.com.au</a></span><br>
							<span class="street-address">10 Easey street, PO Box 1689</span><br>
							<span class="region">Collingwood VIC</span>
							<span class="postal-code">3066</span><br>
							<span class="country-name">Australia</span>
						</p>
					</div>
				</div>
			</div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="/about-us/newsletter/"></a><h5>sign up <br/><span>to our <br/> newsletter</span></h5></div></div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="http://blog.id.com.au"></a><h5>follow <br/><span>our <br/> blog</span></h5></div></div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="http://www.youtube.com/user/informeddecisions"></a><h5>access <br/><span>training <br /> videos</span></h5></div></div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="/public-resources/demographic-resource-centre/"></a><h5>access <br/><span>demographic <br /> resource <br /> centre</span> </h5></div></div>
		</div>
		
	<footer class="main-footer row pad-double">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Sidebar')) : ?>
		<?php endif; ?>
		<?php if ( has_nav_menu( 'footer-menu' ) ) {
			wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'inline-list', 'container' => 'nav', 'container_class' => 'large-12 columns' ) );
		} ?>
	</footer>
	
	<div class="row ">
		<div class="footer-disclaimer">
			<p>	DISCLAIMER: While all due care has been taken to ensure that the content of this website is accurate and current, there may be errors or omissions in it and no legal responsibility is accepted for the information and opinions in this website.
			</p>
		</div>
	</div>
	<?php wp_footer(); ?>
</div> <!-- end wrapper -->
<!-- Start of Async HubSpot Analytics Code -->
	<script type="text/javascript">
	    (function(d,s,i,r) {
	        if (d.getElementById(i)){return;}
	        var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
	        n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/320463.js';
	        e.parentNode.insertBefore(n, e);
	    })(document,"script","hs-analytics",300000);
	</script>
<!-- End of Async HubSpot Analytics Code -->
</body>
</html>