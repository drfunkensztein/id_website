<?php

// Enqueue CSS and scripts

function load_cornerstone_child_styles() {
    wp_enqueue_style('webfont_css', get_stylesheet_directory_uri() . '/css/MyFontsWebfontsKit.css', array('foundation_css'), false, 'all');
	wp_enqueue_style('cornerstone_child_css', get_stylesheet_directory_uri() . '/style.css', array('foundation_css'), false, 'all');
    wp_enqueue_style('responsive_css', get_stylesheet_directory_uri() . '/css/responsive.css', array('foundation_css'), false, 'all');
}

function load_specific_child_styles() {


	if (is_page('location-analysis-placemaker')) {
        wp_enqueue_style('placemaker', get_stylesheet_directory_uri() . '/css/placemaker.css', array('foundation_css'), false, 'all');
    }
    else if (is_page('safi') || is_page('population-forecasting-safi')) {
        wp_enqueue_style('safi', get_stylesheet_directory_uri() . '/css/safi.css', array('foundation_css'), false, 'all');
    }
    

}

function load_common_scripts(){
     wp_register_script( 'main', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'));
     wp_register_script( 'plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array('jquery'));
     wp_enqueue_script('plugins');
     wp_enqueue_script('main');
}

function load_page_specific_scripts() {
    global $post;
    wp_register_script( 'isotop', get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'));
    wp_register_script( 'iscroll', get_stylesheet_directory_uri() . '/js/iscroll-5.js', array('jquery'));
    wp_register_script( 'masonry', get_stylesheet_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'));
    wp_register_script( 'placemaker', get_stylesheet_directory_uri() . '/js/placemaker.js', array('jquery'));
    wp_register_script( 'safi', get_stylesheet_directory_uri() . '/js/safi.js', array('jquery'));
    wp_register_script( 'resonsiveTables', get_stylesheet_directory_uri() . '/js/responsive-tables.js', array('jquery'));
    wp_register_script( 'clientlist', get_stylesheet_directory_uri() . '/js/clientlist.js', array('jquery'));
    wp_register_script( 'staff', get_stylesheet_directory_uri() . '/js/staff.js', array('jquery'));

    if( is_page() || is_single() )
    {
        switch($post->post_name) 
        {
            case 'location-analysis-placemaker':
                wp_enqueue_script('iscroll');
                wp_enqueue_script('masonry');
                wp_enqueue_script('placemaker');
                break;
            case 'safi':
            case 'population-forecasting-safi':
                wp_enqueue_script('iscroll');
                wp_enqueue_script('masonry');
                wp_enqueue_script('safi');
                break;
            case 'meet-the-team':
                wp_enqueue_script('isotop');
                wp_enqueue_script('staff');
                break;
            case 'clients':
                //wp_enqueue_script('clientlist');
                break;
        }
    } 
}

add_action('wp_enqueue_scripts', 'load_cornerstone_child_styles',50);
add_action('wp_enqueue_scripts', 'load_specific_child_styles',50);
add_action('wp_enqueue_scripts', 'load_common_scripts');
add_action('wp_enqueue_scripts', 'load_page_specific_scripts');


?>