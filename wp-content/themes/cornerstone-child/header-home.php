<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <title><?php wp_title(''); ?></title>
  
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="http://home.id.com.au/favicon.ico" >

  <!--[if lt IE 9]>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script type="text/javascript" src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
    <script type="text/javascript" src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
    <script type="text/javascript" src="wp-content/themes/cornerstone-child/js/rem.min.js"></script>
  <![endif]--> 

  <?php wp_head(); ?>

  <script type="text/javascript">
  jQuery(function(){
    ID.HOME.init();
  })
    
  </script>

</head>

<body <?php body_class(); ?>>
<div class="fixed">
  <nav class="row main-navigation top-bar" data-topbar>
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
      </li>
      <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
      <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
    </ul>

    <section class="top-bar-section">
      <?php
        // Left Nav Section
        if ( has_nav_menu( 'header-menu-left' ) ) {
            wp_nav_menu( array(
                'theme_location' => 'header-menu-left',
                'container' => false,
                'depth' => 0,
                'items_wrap' => '<ul class="left">%3$s</ul>',
                'fallback_cb' => false,
                'walker' => new cornerstone_walker( array(
                    'in_top_bar' => true,
                    'item_type' => 'li'
                ) ),
            ) );
          }
        ?>

      <?php
        //Right Nav Section
        if ( has_nav_menu( 'header-menu-right' ) ) {
            wp_nav_menu( array(
                'theme_location' => 'header-menu-right',
                'container' => false,
                'depth' => 0,
                'items_wrap' => '<ul class="right">%3$s</ul>',
                'fallback_cb' => false,
                'walker' => new cornerstone_walker( array(
                    'in_top_bar' => true,
                    'item_type' => 'li'
                ) ),
            ) );
          }
        ?>
    </section>
  </nav>
  <div id="top-shadow"></div>
</div>
<div class="row wrapper clearfix">