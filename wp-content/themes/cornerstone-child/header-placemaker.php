<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

  <title><?php wp_title(''); ?></title>
	<meta charset="utf-8" />
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="initial-scale=1.0" />
  <link rel="shortcut icon" href="http://home.id.com.au/favicon.ico" >

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="fixed">
  <nav class="main-navigation top-bar" data-topbar>
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
      </li>
      <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
      <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
    </ul>

    <section class="top-bar-section">
      <?php
        // Left Nav Section
        if ( has_nav_menu( 'header-menu-left' ) ) {
            wp_nav_menu( array(
                'theme_location' => 'header-menu-left',
                'container' => false,
                'depth' => 0,
                'items_wrap' => '<ul class="left">%3$s</ul>',
                'fallback_cb' => false,
                'walker' => new cornerstone_walker( array(
                    'in_top_bar' => true,
                    'item_type' => 'li'
                ) ),
            ) );
          }
        ?>

      <?php
        //Right Nav Section
        if ( has_nav_menu( 'header-menu-right' ) ) {
            wp_nav_menu( array(
                'theme_location' => 'header-menu-right',
                'container' => false,
                'depth' => 0,
                'items_wrap' => '<ul class="right">%3$s</ul>',
                'fallback_cb' => false,
                'walker' => new cornerstone_walker( array(
                    'in_top_bar' => true,
                    'item_type' => 'li'
                ) ),
            ) );
          }
        ?>
    </section>
  </nav>
  <div id="top-shadow"></div>
</div>