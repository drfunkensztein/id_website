<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="http://home.id.com.au/favicon.ico" >
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
    <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
  <![endif]-->
  
  

  <title><?php wp_title(''); ?></title>

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="fixed">
  <nav class="row main-navigation top-bar <?php echo get_field('clientproduct'); ?>" data-topbar>
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
        <a href="<?php //echo home_url( '/' ); ?>" title="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php //bloginfo( 'name' ); ?></a>
      </li>
      <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
      <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
    </ul>

    <section class="top-bar-section">
      <?php
        // Left Nav Section
        if ( has_nav_menu( 'header-menu-left' ) ) {
            wp_nav_menu( array(
                'theme_location' => 'header-menu-left',
                'container' => false,
                'depth' => 0,
                'items_wrap' => '<ul class="left">%3$s</ul>',
                'fallback_cb' => false,
                'walker' => new cornerstone_walker( array(
                    'in_top_bar' => true,
                    'item_type' => 'li'
                ) ),
            ) );
          }
        ?>

      <?php
        //Right Nav Section
        if ( has_nav_menu( 'header-menu-right' ) ) {
            wp_nav_menu( array(
                'theme_location' => 'header-menu-right',
                'container' => false,
                'depth' => 0,
                'items_wrap' => '<ul class="right">%3$s</ul>',
                'fallback_cb' => false,
                'walker' => new cornerstone_walker( array(
                    'in_top_bar' => true,
                    'item_type' => 'li'
                ) ),
            ) );
          }
        ?>
    </section>
  </nav>
  <div id="top-shadow"></div>
</div>
<div class="row wrapper">
<!-- logo -->
<div class="row header">
  <div class="id-logo <?php echo get_field('clientproduct'); ?>">
    <a href="/"></a>
  </div>
    <!-- AddThis Button BEGIN -->
    <div id="addThisBtns" class="addthis_toolbox addthis_default_style idc-share idc-share-small">
        <a class="idc-pagePdf" href="http://profile.id.com.au/albany/Reporter/Create?id=home&amp;format=1"></a>
        <a class="addthis_button_google_plusone_share at300b" style="cursor: pointer" href="http://www.addthis.com/bookmark.php?v=300&amp;winname=addthis&amp;pub=ra-503ae79e5abcd091&amp;source=tbx-300&amp;lng=en-US&amp;s=google_plusone_share&amp;url=http%3A%2F%2Fprofile.id.com.au%2Falbany&amp;title=Community%20profile%20%7C%20City%20of%20Albany%20%7C%20profile.id&amp;ate=AT-ra-503ae79e5abcd091/-/-/52d5e5abcf08cb09/2&amp;frommenu=1&amp;uid=52d5e5abdcaf3893&amp;ct=1&amp;pre=http%3A%2F%2Fprofile.id.com.au%2F&amp;tt=0&amp;captcha_provider=nucaptcha" target="_blank" title="Google+"><span class="at16nc at300bs at15nc at15t_google_plusone_share at16t_google_plusone_share"><span class="at_a11y">Share on google_plusone_share</span></span></a>
        <a class="addthis_button_twitter at300b" style="cursor: pointer" title="Tweet" href="#"><span class="at16nc at300bs at15nc at15t_twitter at16t_twitter"><span class="at_a11y">Share on twitter</span></span></a>
        <a class="addthis_button_facebook at300b" style="cursor: pointer" title="Facebook" href="#"><span class="at16nc at300bs at15nc at15t_facebook at16t_facebook"><span class="at_a11y">Share on facebook</span></span></a>
        <a class="addthis_button_linkedin at300b" style="cursor: pointer" href="http://www.addthis.com/bookmark.php?v=300&amp;winname=addthis&amp;pub=ra-503ae79e5abcd091&amp;source=tbx-300&amp;lng=en-US&amp;s=linkedin&amp;url=http%3A%2F%2Fprofile.id.com.au%2Falbany&amp;title=Community%20profile%20%7C%20City%20of%20Albany%20%7C%20profile.id&amp;ate=AT-ra-503ae79e5abcd091/-/-/52d5e5abcf08cb09/3&amp;frommenu=1&amp;uid=52d5e5abe4ad1c11&amp;ct=1&amp;pre=http%3A%2F%2Fprofile.id.com.au%2F&amp;tt=0&amp;captcha_provider=nucaptcha" target="_blank" title="LinkedIn"><span class="at16nc at300bs at15nc at15t_linkedin at16t_linkedin"><span class="at_a11y">Share on linkedin</span></span></a>
      <div class="atclear"></div>
    </div>
    <script src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-503ae79e5abcd091" async=""></script>
    <!-- AddThis Button END -->
</div>
