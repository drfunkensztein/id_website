(function ($, window, document, undefined) {

var clientListTemplate = null;
//var clientProduct = '';
$(document).ready(function () {
	loadClientList(clientProduct, "All");
	//console.log("clientProduct: ", clientProduct)
	$('.clientFilterRegion').bind('click', function (e) {
		loadClientList(clientProduct, $(this).html());
		$('button').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
});



function loadClientList(product, region) {

	var url = 'http://data.id.com.au/flint/Products.svc/web/clientsjson?'; //'http://dubai/flint/Products.svc/web/clientsjson';'http://date.jsontest.com'; // http://home.id.com.au/rest/clients?product=profile&state=NSW
	
	$('.loader').removeClass('hidden').show();

	$.get(url, {product:product, state:region}, function (data) {
		
		if (data.length > 0){
			processData(data);
		} 
		else {
			var newClientHtml = "";
			var container = $('.clientListContainer');
			var message = 'There are no matches for this area.';
			if ($(container).get(0).nodeName == 'tr') {
				newClientHtml = '<tr class="no-match"><td colspan="5">' + message + '</td></tr>';
			} else {
				newClientHtml = '<tr class="no-match"><td colspan="5">' + message + '</td></tr>';;
			}
			$(container).html(newClientHtml);
			$('.loader').hide();
			$(container).removeClass('hidden');
		}

		
	});
}

function processData(data, newClientHtml){

	var container = $('.clientListContainer'),
		newClientHtml ="";
	if (clientListTemplate == null) {
		clientListTemplate = container.html();
	}
	
	$(data).each(function (index) {

		var productHtml = clientListTemplate,
			profileUrl 	= this.u_profile,
			atlasUrl 	= this.u_atlas,
			forecastUrl = this.u_forecast,
			economyUrl 	= this.u_economy
		
		if (this.u_client == '') {
			productHtml = productHtml.replace('{clientWithAreaName}', '{areaName}');
		} else {
			productHtml = productHtml.replace('{clientWithAreaName}', '<a href="{clientUrl}" target="_blank">{areaName}</a>');
		}

		productHtml = productHtml.replace('{clientName}'			, this.clientname);
		productHtml = productHtml.replace('{areaName}'				, this.areaname);
		productHtml = productHtml.replace('{clientUrl}'				, this.u_client);
		productHtml = productHtml.replace('{highlightClassAppend}'	, (index % 2 == 0 ? '' : '-alt'));
		productHtml = productHtml.replace('{highlightClass}'		, (index % 2 == 0 ? '' : 'alt'));
		productHtml = productHtml.replace('{productProfile}'		, (profileUrl != null ? '<a class="link-arrow"  href="' + cleanUrl(profileUrl) + '" target="_blank">&nbsp;</a>' : ''));
		productHtml = productHtml.replace('{productAtlas}'			, (atlasUrl != null ? '<a class="link-arrow" href="' + cleanUrl(atlasUrl) + '" target="_blank">&nbsp;</a>' : ''));
		productHtml = productHtml.replace('{productForecast}'		, (forecastUrl != null ? '<a class="link-arrow" href="' + cleanUrl(forecastUrl) + '" target="_blank">&nbsp;</a>' : ''));
		productHtml = productHtml.replace('{productEconomy}'		, (economyUrl != null ? '<a class="link-arrow" href="' + cleanUrl(economyUrl) + '" target="_blank">&nbsp;</a>' : ''));

		productHtml = productHtml.replace((($.browser.mozilla && $.browser.version < 15) ? escape('{profileUrl}') 	: '{profileUrl}')	, cleanUrl(profileUrl));
		productHtml = productHtml.replace((($.browser.mozilla && $.browser.version < 15) ? escape('{atlasUrl}') 	: '{atlasUrl}')		, cleanUrl(atlasUrl));
		productHtml = productHtml.replace((($.browser.mozilla && $.browser.version < 15) ? escape('{forecastUrl}') 	: '{forecastUrl}')	, cleanUrl(forecastUrl));
		productHtml = productHtml.replace((($.browser.mozilla && $.browser.version < 15) ? escape('{economyUrl}') 	: '{economyUrl}')	, cleanUrl(economyUrl));

		newClientHtml += productHtml;
	});

	container.html(newClientHtml);
	$('.loader').hide();
	container.removeClass('hidden');

}

function cleanUrl(value) {
	// Append http:// if not already on the URL
	if (value != null && !value.match(/^http(s)?:/i)) {
		value = 'http://' + value;
	}
	return value;
}

})(jQuery, window, document);	
