(function ($, window, document, undefined) {

	var ID = {};
	
	ID = {
		init:function(){
			var self = this;
			// On DOM ready
			$(function(){
				self.scroll.init();
				self.videos.init();

				// enforce relayout which doesn't seem to work between 959 and 1024 when resizing very slowwwwwly.
				Response.create({ 
				    prop: "width" // property to base tests on
				  , prefix: "r src" // custom aliased prefixes
				  , breakpoints: [0, 320, 481, 768, 801, 959, 1024] // custom breakpoints
				  , lazy: true // enable lazyloading
				});

				$(window).resize(function(){
					ID.utils.centerInViewport();
				}).resize();
				
				// blog feeder
				$(".blog-feed").rssfeed('http://blog.id.com.au/feed/', {
					limit: 2,
					dateformat: 'date',
					date :false,
					titletag : "h6",
					header : false,
					content :false,
					snippet: false
					},function(e) {
					$("h6 a",e).each(function(i) {
						var title = $(this).text();
						if (title.length > 50) $(this).text(title.substring(0,50) + '...');
					});
				});
			});
		},
		scroll : {
			init:function(){
				$(window).on('scroll', function(){
					var y = $(window).scrollTop();
					if( y > 0 ){
						$("#top-shadow").css({'display':'block', 'opacity':y/20});
					} 
					else {
						$("#top-shadow").css({'display':'block', 'opacity':y/20});
					}
				});
			}
		},
		videos : {
			init : function(){
				// youtube overlay
				$('.playVideo a').click(function(e) {
					e.preventDefault();
					youtubeSnippet = $(this).attr('href');
					$('body').prepend('<div id="overlay">&nbsp;</div><div id="youtube-box"></div>');
					data = replaceYoutubeCode(youtubeSnippet);
					$('#youtube-box').html(data);
					ID.utils.centerInViewport();
					showOverlay();
				});

				function replaceYoutubeCode(str){
					//console.log("replacing: ", str)
					return str.replace(/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, '<iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>');
				}

				function showOverlay() {
				    $('#overlay').css({display: 'block'});
				    windowHeight = $(window).height();
					totalHeight = $('.wrapper').height();
					if(windowHeight < totalHeight) {
						totalHeight = totalHeight + 44;
					}
				    $('#overlay').css({height: totalHeight+'px'});
				    //$(window).scrollTo(0, 800);
				    $('#overlay').animate({opacity: 0.5}, 300, "swing", function() {
				    	$('#overlay').on("click", function(){closeOverlay();})
				    });
				}

				function closeOverlay() {
				    $('#youtube-box').animate({opacity: 0.0}, 100, "swing", function() {
				    	$('#overlay').animate({opacity: 0.0}, 100, "swing", function() {
				        	$('#overlay').remove();
				            $('#youtube-box').remove();
				    	});
				    });		    
				}					
			}
		},
		slider : {
			init:function(){
				console.log('slider init');
				$('.flexslider').flexslider({
				    animation: "slide",
				    easing: "swing",
				    pauseOnHover: true,
				    touch: false,
				    slideshowSpeed: 6000,
				    useCSS: true
				});
			}
		}
	};

	ID.HOME = {
		init:function(){
			var self = this;
			$(function(){
				ID.slider.init();
			});
		},
	};

	ID.utils = {
		centerInViewport : function(){
			// get the screen height and width  
			var maskHeight = $(window).height();  
			var maskWidth = $(window).width();

			// calculate the values for center alignment
			var dialogTop =  (maskHeight  - $('#youtube-box').height())/2;  
			var dialogLeft = (maskWidth - $('#youtube-box').width())/2; 

			// assign values to the overlay and dialog box
			$('#overlay').css({ height:$(document).height(), width:$(document).width() }).show();
		    $('#youtube-box').css({ top: dialogTop, left: dialogLeft, position:"fixed"}).show();
		}
	}

	ID.init();

	window.ID = ID;
})(jQuery, window, document);