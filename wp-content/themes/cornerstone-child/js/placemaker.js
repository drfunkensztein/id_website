/* Author:  anthonym */
(function ($, window, document, undefined) {
var PLACEMAKER = {}, project = "wg";
	PLACEMAKER[project] = {

		init: function() {
			var self = this;

			$(function(){

				self.quotes.init();

				self.location.init();

				self.anchors.init();

				$('a.vid').on('click', function (e) {
					e.preventDefault();
					PLACEMAKER.utils.showOverlay('<iframe width="805" height="453" src="//www.youtube.com/embed/cxA9SJmydeI?rel=0" frameborder="0" allowfullscreen></iframe>');
				});

				if($('.lt-ie10').length) {
					$('input[type="phone"], input[type="text"], input[type="email"]').addPlaceholder();
				}

				$('.case-studies-container').masonry();

			});
		},

		location : {
			active: false,
			init : function () {

				var self = this;

				if($('.lt-ie10').length) {
					this.activateIE();
				}

				$(window).on('scroll', function (e) {

					e.preventDefault();

					if($(this).scrollTop() > 1305 && self.active === false) {
						self.active === true;
						if($('.lt-ie10').length) {
							self.startIE();
						} else {
							self.start();
						}
					}

				});
			},
			start : function () {

				var self = this;

				//$(window).off('scroll');

				$('.animation').removeClass('animation-waiting').addClass('animation-active');

			},
			activateIE : function () {

				var self = this;

				this.$animContainer = $('.animation');
				this.$text = $('.text', this.$animContainer);

				this.$layers = $('.layer', this.$animContainer);
				this.$layer1 = $('.layer-1', this.$animContainer);
				this.$layer2 = $('.layer-2', this.$animContainer);
				this.$layer3 = $('.layer-3', this.$animContainer);

				this.$text.css({opacity: 0});
				$('.line', this.$text).css({opacity: 0});

				this.$layers.css({left: '100%'});

				this.$layer1.css({top: 150});
				this.$layer3.css({top: 150});



			},
			startIE : function () {

				var self = this;

				$(window).off('scroll');

				$('.animation').removeClass('animation-waiting');

				this.$layers.animate({left: 0}, {
					duration: 600,
					easing: 'easeInOutExpo',
					complete:function () {
						self.spreadLayers();
					}
				});
			},
			spreadLayers : function () {

				this.$layer1.stop(false,false).animate({top: 300}, {
					duration: 600,
					easing: 'easeInOutExpo'
				});

				this.$layer3.stop(false,false).animate({top: 0}, {
					duration: 600,
					easing: 'easeInOutExpo'
				});

				this.$text.animate({opacity : 1}, {
					duration : 1000,
					easing: 'linear',
					complete: function () {
						$('.line', $(this)).animate({opacity : 1}, {
							duration: 200,
							easing: 'linear'
						})
					}
				})

			}

		},

		mobThreshWidth : 766,
		anchors : {
			init : function () {

				var self = this;

				$('.anchor').on('click', function (e) {

					e.preventDefault();

					if(typeof $(this).data('section') !== 'undefined') {
						var $target = $('.' +  $(this).data('section'));

						$('html, body').animate({scrollTop : $target.offset().top - 35}, {
							duration : 400,
							easing: 'easeInOutExpo'
						})

					}

				});
			}
		},

		quotes : {
			featureTimeout : null,
			featureWait : 6000,
			init: function() {

				var self = this;

				self.$quotes = $('.pm-quotes');

				self.activateSlides();

			},
			activateSlides: function () {

				if(!$('.pm-quotes').length || $('.slide').length < 2) {
					return;
				}

				var self = this;

				$(window).on('resize', function () {
					self.update($(this).width());
				});

				if($('.lt-ie10').length) {
					this.activateIE();
					return;
					this.update($(window).width());
				}

				this.scroll = new IScroll('.quote-scroller', {
					snap: true,
					momentum: false,
					lockDirection : false,
					scrollX: true,
					scrollY: false,
					eventPassthrough: true,
					hScrollbar: false
				});

				this.scroll.on('scrollEnd', function () {
					self.updatePagination(this.currentPage.pageX);
				});

				$('.quote-dots a', this.$quotes).on('click', function (e) {
					e.preventDefault();
					self.scroll.goToPage($(this).index('.quote-dots a'));
				});

				this.update($(window).width());

			},
			update: function (width) {
				var self = this;

				$('.slide', this.$quotes).width(width);
				$('.container', this.$quotes).width((width * $('.slide', this.$quotes).length) + 10);

				if(typeof self.scroll !== 'undefined') {
					self.scroll.refresh();
				}
				if($('.lt-ie10').length) {
					var offset  = -1 * ($('.quote-dots .active a').index('.quote-dots a') * this.$quotes.width());
					$('.container', this.$quotes).css({left : offset });
				}
			},
			activateIE : function () {

				var self = this;

				$('.quote-dots a', this.$quotes).on('click', function (e) {
					e.preventDefault();
					$('.container', self.$quotes).stop(true, true);
					self.goTo($(this).index('.quote-dots a'));
				});

			},
			goTo : function (index) {

				var offset = index * this.$quotes.width();

				$('.container', this.$quotes).animate({left: -1 * offset}, {
					duration : 500,
					easing: 'easeInOutExpo'
				});
				this.updatePagination(index);

			},
			updatePagination : function (newIndex) {

				var self = this;

				$('.quote-dots li', self.$quotes).removeClass('active');
				$('.quote-dots li:eq(' + newIndex + ')', self.$quotes).addClass('active');

			}
		}

	};

	PLACEMAKER.utils = {
		baseHref: (function () {
			var baseTags = document.getElementsByTagName('base');
			if (baseTags !== undefined && baseTags.length > 0) {
				return baseTags[0].href;
			} else {
				return null;
			}
		})(),
		getIndex : function(currentIndex, lastIndex, slide) {

			var newIndex;

			if (typeof slide === 'string') {

				if (slide.match('next')) {

					if (currentIndex === lastIndex) {
						newIndex = 0;
					} else {
						newIndex = currentIndex + 1;
					}

				} else if (slide.match('prev')) {

					if (currentIndex === 0) {
						newIndex = lastIndex;
					} else {
						newIndex = currentIndex - 1;
					}

				}

			} else if (typeof slide === 'number') {

				newIndex = slide;

			} else {
				return;
			}

			return newIndex;

		},
		getAnimDirection : function(newIndex, currentIndex) {
			var direction = 1;

			if (newIndex < currentIndex) {
				direction = -1;
			}

			return direction;
		},
		showOverlay: function($html) {

	        var self = this, windowHeight, totalHeight;
	        $('body').prepend('<div class="popup-overlay">&nbsp;</div><div class="popup-container  popup-iframe-container"><div class="popup-wrapper"><div class="popup-window"><a href="#" id="popup-close">Close</a><div class="popup-window-content"></div></div></div><div class="overlay-close-pane"></div></div>');

	        $('.popup-overlay').css({display: 'block'});
	        $('.popup-container').css({display: 'block'});

			$('.popup-window-content').append($html);


	        windowHeight = $(window).height();
	        totalHeight = $('body').height();
	        if (windowHeight < totalHeight) {
	            windowHeight = totalHeight;
	        }
			$('.popup-wrapper').css({top : 50});
	        $('.popup-overlay, .popup-container').css({height: windowHeight + 'px'});

	        $('.popup-overlay').animate({opacity: 0.7}, 300, "swing", function() {
	            $('.popup-container').animate({opacity: 1}, 300, "swing", function() {

	                if($('.popup-overlay').height() < $('.popup-window-content').height()) {
		                $('.popup-overlay, .popup-container').height($('.popup-window-content').height() + 120);
	                }
	                $('#popup-close, .overlay-close-pane, .popup-window-content').css({cursor: 'pointer'}).click(function(e) {
	                    e.preventDefault();
	                    self.closeOverlay();
	                });

	            });
	        });
	    },
	    closeOverlay: function() {
	        $('.popup-container').animate({opacity: 0}, 200, "swing", function() {
	            $('.popup-overlay').animate({opacity: 0}, 200, "swing", function() {
	            $('.popup-overlay').remove();
	            $('.popup-container').remove();
	            });
	        });
	    },
		createCookie: function(name,value,days) {
			if (days) {
				var date = new Date(), expires;
				date.setTime(date.getTime()+(days*24*60*60*1000));
				expires = "; expires="+date.toGMTString();
			} else {
				var expires = "";
			}
			document.cookie = name+"="+value+expires+"; path=/";
		},
		readCookie: function(name) {
			var nameEQ = name + "=",
				ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
		},
		eraseCookie: function(name) {
			this.createCookie(name,"",-1);
		}
	};

	PLACEMAKER[project].init();

	window.PLACEMAKER = PLACEMAKER;
})(jQuery, window, document);