/* Author:  anthonym */
(function ($, window, document, undefined) {



var SAFI = {};
	SAFI = {

		init: function() {
			var self = this;
			// on document ready
			$(function(){

				self.quotes.init();
				self.anchors.init();
				self.slider.init();
				self.pins.init();

				$('a.vid').on('click', function (e) {
					e.preventDefault();
					SAFI.utils.showOverlay('<iframe width="805" height="453" src="//www.youtube.com/embed/cxA9SJmydeI?rel=0" frameborder="0" allowfullscreen></iframe>');
				});

				if($('.lt-ie10').length) {
					$('input[type="phone"], input[type="text"], input[type="email"]').addPlaceholder();
				}
			});
		},

		slider : {

			mySlider : null,

			init:function(){

				var el = document.querySelector('.odometer'),
					od = new Odometer({
					  el: el,
					  value: $(".slides li:eq("+0+")").data("year"),
					  format: 'd',
					  theme: 'minimal'
					});
				
				$('.slideshow').flexslider({
				    animation: "fade",
				    easing : "linear",
				    startAt : 0,
				    controlNav : false,
				    directionNav : false,
				    pauseOnHover: false,
				    touch: false,
				    slideshowSpeed: 4000,
				    useCSS: false,
				    start: function(slider) {
				    	SAFI.slider.mySlider = slider;
			        },
			        before : function(slider){
			        	var nextSlide = (slider.currentSlide + 1) <= slider.last ? (slider.currentSlide + 1) : 0,
			        		year = $(".slides li:eq("+(nextSlide)+")").data("year");

			        	od.update(year);
			        }
				});
			}
		},

		pins : {

			init: function(){

				var self = this;

				var gallery 	= $('.safi-gallery-container'),
					miniOdo 	= null,
					overlay 	= $('.safi-gallery-overlay'),
					modalPopup 	= $('.modal-popup'),
					closeButton	= $('.close'),
					pin1 		= $(".pin-1"),
					pin2 		= $(".pin-2"),
					pin3 		= $(".pin-3")

				$(window).on('scroll', function (e) {
					e.preventDefault();
					$(".pin-1, .pin-2, .pin-3").visible(true) ? $(".pin-1, .pin-2, .pin-3").find('span').addClass('animated bounce') : $(".pin-1, .pin-2, .pin-3").find('span').removeClass('animated bounce');
				});

				$('.safi-gallery-overlay, .close').on('click', function(){
					SAFI.slider.mySlider.play();
					overlay.add(closeButton).add(modalPopup).fadeOut();
				})

				$('.pin').on('click', function(){
					SAFI.slider.mySlider.pause();
					modalPopup.hide();
					$("." + $(this).data('modal')).add(overlay).add(closeButton).fadeIn();
					self.activateMiniSlideshow($($(this).data('modal')));
				});

				var thether_pin1 = new Tether({
				  element: $("." + pin1.data('modal')),
				  target: pin1,
				  attachment: 'top left',
				  targetAttachment: 'top right',
				  targetOffset: '-210px 40px'
				});

				var thether_pin2 = new Tether({
				  element: $("." + pin2.data('modal')),
				  target: pin2,
				  attachment: 'top left',
				  targetAttachment: 'top right',
				  targetOffset: '-120px 40px'
				});

				var thether_pin3 = new Tether({
				  element: $("." + pin3.data('modal')),
				  target: pin3,
				  attachment: 'top left',
				  targetAttachment: 'top right',
				  targetOffset: '-60px -440px'
				});
			},

			activateMiniSlideshow : function(modal){
				
				var miniOdo 		= null,
					slideselector 	= ".slideshow-" + modal.selector,
					odoselector		= document.querySelector('.odo-' + modal.selector);

				miniOdo = new Odometer({
					auto: false, 
					el: odoselector,
					value: '2011',
					format: '',
					theme: 'default'
				});

				$(slideselector).flexslider({
				    animation: "fade",
				    easing : "linear",
				    startAt : 0,
				    pauseOnHover: false,
				    touch: false,
				    controlNav : false,
				    directionNav : false,
				    slideshowSpeed: 4000,
				    useCSS: false,
			        before : function(slider){
			        	var nextSlide = (slider.currentSlide + 1) <= slider.last ? (slider.currentSlide + 1) : 0,
			        		year = $(".slideshow-" + modal.selector + " .slides li:eq("+(nextSlide)+")").data("year");
			        	miniOdo.update(year);
			        }
				});
			}
		},

		mobThreshWidth : 766,
		anchors : {
			init : function () {

				var self = this;

				$('.anchor').on('click', function (e) {
					e.preventDefault();
					if(typeof $(this).data('section') !== 'undefined') {
						var $target = $('.' +  $(this).data('section')),
							position = $target.offset().top - 35;

						$('html, body').animate({scrollTop : position}, {
							duration : 400,
							easing: 'easeInOutExpo'
						})
					}
				});
			}
		},

		quotes : {
			featureTimeout : null,
			featureWait : 6000,
			init: function() {

				var self = this;

				self.$quotes = $('.pm-quotes');

				self.activateSlides();

			},
			activateSlides: function () {

				if(!$('.pm-quotes').length || $('.slide').length < 2) {
					return;
				}

				var self = this;

				$(window).on('resize', function () {
					self.update($(this).width());
				});

				if($('.lt-ie10').length) {
					this.activateIE();
					return;
					this.update($(window).width());
				}

				this.scroll = new IScroll('.quote-scroller', {
					snap: true,
					momentum: false,
					lockDirection : false,
					scrollX: true,
					scrollY: false,
					eventPassthrough: true,
					hScrollbar: false
				});

				this.scroll.on('scrollEnd', function () {
					self.updatePagination(this.currentPage.pageX);
				});

				$('.quote-dots a', this.$quotes).on('click', function (e) {
					e.preventDefault();
					self.scroll.goToPage($(this).index('.quote-dots a'));
				});

				this.update($(window).width());

			},
			update: function (width) {
				var self = this;

				$('.slide', this.$quotes).width(width);
				$('.container', this.$quotes).width((width * $('.slide', this.$quotes).length) + 10);

				if(typeof self.scroll !== 'undefined') {
					self.scroll.refresh();
				}
				if($('.lt-ie10').length) {
					var offset  = -1 * ($('.quote-dots .active a').index('.quote-dots a') * this.$quotes.width());
					$('.container', this.$quotes).css({left : offset });
				}
			},
			activateIE : function () {

				var self = this;

				$('.quote-dots a', this.$quotes).on('click', function (e) {
					e.preventDefault();
					$('.container', self.$quotes).stop(true, true);
					self.goTo($(this).index('.quote-dots a'));
				});

			},
			goTo : function (index) {

				var offset = index * this.$quotes.width();

				$('.container', this.$quotes).animate({left: -1 * offset}, {
					duration : 500,
					easing: 'easeInOutExpo'
				});
				this.updatePagination(index);

			},
			updatePagination : function (newIndex) {

				var self = this;

				$('.quote-dots li', self.$quotes).removeClass('active');
				$('.quote-dots li:eq(' + newIndex + ')', self.$quotes).addClass('active');
			}
		}

	};

	SAFI.utils = {
		baseHref: (function () {
			var baseTags = document.getElementsByTagName('base');
			if (baseTags !== undefined && baseTags.length > 0) {
				return baseTags[0].href;
			} else {
				return null;
			}
		})(),
		getIndex : function(currentIndex, lastIndex, slide) {

			var newIndex;

			if (typeof slide === 'string') {

				if (slide.match('next')) {

					if (currentIndex === lastIndex) {
						newIndex = 0;
					} else {
						newIndex = currentIndex + 1;
					}

				} else if (slide.match('prev')) {

					if (currentIndex === 0) {
						newIndex = lastIndex;
					} else {
						newIndex = currentIndex - 1;
					}

				}

			} else if (typeof slide === 'number') {

				newIndex = slide;

			} else {
				return;
			}

			return newIndex;

		},
		getAnimDirection : function(newIndex, currentIndex) {
			var direction = 1;

			if (newIndex < currentIndex) {
				direction = -1;
			}

			return direction;
		},
		showOverlay: function($html) {

	        var self = this, windowHeight, totalHeight;
	        $('body').prepend('<div class="popup-overlay">&nbsp;</div><div class="popup-container  popup-iframe-container"><div class="popup-wrapper"><div class="popup-window"><a href="#" id="popup-close">Close</a><div class="popup-window-content"></div></div></div><div class="overlay-close-pane"></div></div>');

	        $('.popup-overlay').css({display: 'block'});
	        $('.popup-container').css({display: 'block'});

			$('.popup-window-content').append($html);


	        windowHeight = $(window).height();
	        totalHeight = $('body').height();
	        if (windowHeight < totalHeight) {
	            windowHeight = totalHeight;
	        }
			$('.popup-wrapper').css({top : 50});
	        $('.popup-overlay, .popup-container').css({height: windowHeight + 'px'});

	        $('.popup-overlay').animate({opacity: 0.7}, 300, "swing", function() {
	            $('.popup-container').animate({opacity: 1}, 300, "swing", function() {

	                if($('.popup-overlay').height() < $('.popup-window-content').height()) {
		                $('.popup-overlay, .popup-container').height($('.popup-window-content').height() + 120);
	                }
	                $('#popup-close, .overlay-close-pane, .popup-window-content').css({cursor: 'pointer'}).click(function(e) {
	                    e.preventDefault();
	                    self.closeOverlay();
	                });

	            });
	        });
	    },
	    closeOverlay: function() {
	        $('.popup-container').animate({opacity: 0}, 200, "swing", function() {
	            $('.popup-overlay').animate({opacity: 0}, 200, "swing", function() {
	            $('.popup-overlay').remove();
	            $('.popup-container').remove();
	            });
	        });
	    },
		createCookie: function(name,value,days) {
			if (days) {
				var date = new Date(), expires;
				date.setTime(date.getTime()+(days*24*60*60*1000));
				expires = "; expires="+date.toGMTString();
			} else {
				var expires = "";
			}
			document.cookie = name+"="+value+expires+"; path=/";
		},
		readCookie: function(name) {
			var nameEQ = name + "=",
				ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
		},
		eraseCookie: function(name) {
			this.createCookie(name,"",-1);
		}
	};

	SAFI.init();

	window.SAFI = SAFI;
})(jQuery, window, document);