
	jQuery(function(){

	   // Staff page mechanic
		var $container = jQuery('.staff-members'),
			$staff = jQuery('.staff-member');

		Response.crossover(function(e){ 
			$container.isotope({
			  layoutMode: Response.band(959) ? 'fitRows' : 'masonry'
			});
		}, "width");

		jQuery('.staffFilter').bind('click', function (e) {
			jQuery('button').removeClass('active');
			jQuery(this).addClass('active');
			e.preventDefault();
		});

		$staff.on("click", function(e){
			$staff.not(this).removeClass("expanded large-8");
			jQuery(this).toggleClass("expanded large-8");
			$container.isotope();
		})

		if($container.length){
			$container.isotope({
			  isResizeBound : true,
			  itemSelector: '.staff-member',
			  layoutMode: 'masonry'
			});
		}

		// filter items on button click
		jQuery('#staff-filters').on( 'click', 'button', function( event ) {
		  var filtr = jQuery(this).attr('data-filter');
		  $container.isotope({ filter: filtr });
		});

	});