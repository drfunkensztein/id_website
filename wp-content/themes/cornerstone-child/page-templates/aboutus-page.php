<?php
/**
 * Template Name: About us page
 *
 * Description: The about us page.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header(); ?>
<div class="row pad-double">
	
	<div id="content" class="large-12 columns" role="main">
		<div class="row pad-double">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>
			<?php endwhile; ?>		
		</div>

		<div class="row pad sub-heroes ">
			
			<div class="large-9 medium-9">
				<div class="about-box-alt medium-6 small-12 large-6 columns">
					<div class="clickable about-box-container contact-phone">
						<h1>.id community</h1>
						<p>We develop online information products for government decision makers.</p>
						<p>Learn more >></p>
						<a href="/services/local-government-products/"></a>
					</div>
				</div>

				<div class="about-box medium-6 small-12 large-6 columns">
					<div class="clickable about-box-container contact-phone">
						<h1>.id placemaker</h1>
						<p>.id placemaker combines our population insights with  your business data, into an easy to use, powerful cloud-based platform designed for decisionmakers.</p>
						<p>Learn more >></p>
						<a href="/services/location-analysis-placemaker/"></a>
					</div>
				</div>
				<div class="about-box medium-6 small-12 large-6 columns">
					<div class="clickable about-box-container contact-phone">
						<h1>.id forecasts</h1>
						<p>Our Small Area Forecast information (SAFi) lets you acheive unprecedent insights into the future of Australia's populations, with the reliability you need to make important decisions.</p>
						<p>Learn more >></p>
						<a href="/services/population-forecasting-safi/"></a>
					</div>
				</div>
				<div class="about-box-alt medium-6 small-12 large-6 columns">
					<div class="clickable about-box-container contact-phone">
						<h1>.id consulting</h1>
						<p>As population experts, our job is to tell the stories about the past, present and future of populations and places, and help organisations respond to change.</p>
						<p>Learn more >></p>
						<a href="/services/consulting-services/"></a>
					</div>
				</div>

			</div>

			
			
		</div>

		<!-- CONTENT BOXES -->
		<div class="row pad sub-heroes">
			<?php while(has_sub_field("sub_hero_box")): ?>
				<!-- Generic box -->
				<?php if(get_row_layout() == "generic_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
			 		<div class="sub-hero home-box medium-4 small-12 large-4 columns">
						<div class="generic <?php if (get_sub_field('is_page_link')) echo 'clickable' ?>" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div><?php the_sub_field("content"); ?></div>
							<?php if (get_sub_field('is_page_link')):?>
								<a href="<?php echo get_sub_field('page_link') ?>"></a>
							<?php endif; ?>
				 		</div>
					 </div>
				<!-- Blogroll box -->
				<?php elseif(get_row_layout() == "blogroll_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="blogroll" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="blog-feed"></div>
							<div>
								<h6 class="more-blog-posts"><a href="http://blog.id.com.au">see more blog posts</a></h6>
							</div>
				 		</div>
					 </div>
				<!-- Video box -->	 
				<?php elseif(get_row_layout() == "video_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="video" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="playVideo">
								<a href="<?php echo get_sub_field('youtube_video') ?>"><img class="image" src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt=""></a>
							</div>
				 		</div>
					 </div>
				<!-- Subscribe box -->
				<?php elseif(get_row_layout() == "subscribe_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="subscribe" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="subscribe-form">
								<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>

								<script>
								  hbspt.forms.create({ 
								    portalId: '320463',
								    formId: '95441755-778f-41b8-b170-c4551c7b4396',
								    formData: {
							            cssClass: ''
							        },
							        validationOptions : {
							        	position : 'center left',
							        	messageClass : 'err'
							        },
							        onBeforeValidationInit:function(form){
							        	jQuery(form).validate();
							        }
								  });
								</script>
							</div>
				 		</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</div>



	<?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>