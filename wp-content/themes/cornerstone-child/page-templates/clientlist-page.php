<?php
/**
 * Template Name: Client list page
 *
 * Description: The Client list page, no sidebar.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header(); ?>

<div class="row pad-double">
	
	<div id="content" class="large-12 columns" role="main">
		<div class="row pad-double">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>
			<?php endwhile; ?>
		</div>
		<div id="results"></div>
		<div class="row pad-double">
			<div id="region-filters" class="filter-container row button-group pad-double sort">
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion active">ALL</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">ACT</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">NSW</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">NT</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">QLD</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">SA</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">TAS</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">VIC</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">WA</button>
				<button class="triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">NZ</button>
			</div>
			<table class="client-list-header">
				<tbody>
  				  	<tr>
	      				<th class="client-col-head"></th>
	      				<th class="product-col-head"><span class="product-short">pro</span><span class="product">profile</span><span class="id-suffix">.id</span></th>
			            <th class="product-col-head"><span class="product-short">atl</span><span class="product">atlas</span><span class="id-suffix">.id</span></th>
			            <th class="product-col-head"><span class="product-short">for</span><span class="product">forecasts</span><span class="id-suffix">.id</span></th>
			            <th class="product-col-head"><span class="product-short">eco</span><span class="product">economy</span><span class="id-suffix">.id</span></th>
    				</tr>
    			</tbody>
			</table>
			<div class="client-list-container">
				<table class="client-list">
					<tbody class="clientListContainer hidden">
						<tr>
		      				<td class="client-col">{clientName}</td>
				            <td class="product-col">{productProfile}</td>
				            <td class="product-col">{productAtlas}</td>
				            <td class="product-col">{productForecast}</td>
				            <td class="product-col">{productEconomy}</td>
	    				</tr>
					</tbody>
				</table>	
			</div>
			
		</div>
	</div>
	
	<script type="text/javascript">
		var clientProduct = '';
	</script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . '/js/clientlist.js' ?> "></script>

</div>
<?php get_footer(); ?>