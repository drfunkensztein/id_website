<?php
/**
 * Template Name: Contact page
 *
 * Description: The contact page.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header(); ?>
<div class="row pad-double">
	
	<div id="content" class="large-12 columns" role="main">
		<div class="row pad-double">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>
			<?php endwhile; ?>		
		</div>

		<div class="row pad sub-heroes">
			<!-- <div class="sub-hero home-box medium-4 small-12 large-4 columns">
		 	
			</div> -->
			<div class="contact-box medium-4 small-12 large-4 columns">
				<div class="contact-box-container contact-phone">
					<h1>phone</h1>
					<h2 class="phoneNumber">+61 3 9417 2205</h2>
					<h2 class="phoneNumberNZ">NZ freecall: 0800 955 481</h2>
				</div>
			</div>

			<div class="contact-box-alt medium-4 small-12 large-4 columns">
				<div class="clickable contact-box-container contact-email">
					<h1>email</h1>
					<h2>info@id.com.au</h2>
					<p>If you can't find the information you are looking for, send us an email and we'll respond within 24hours. >></p>
					<a href="mailto:info@id.com.au"></a>
				</div>
			</div>

			<div class="clickable contact-box medium-4 small-12 large-4 columns">
				<div class="contact-box-container contact-popin">
					<h1>pop in</h1>
					<address>
						10 Easy street, <br>
						PO Box 1689<br>
						Collingwood,<br>
						Victoria 3066<br>
						Australia
					</address>
					<a href="https://maps.google.com.au/maps?q=10+easey+street+collingwood&ie=UTF-8&hq=&hnear=0x6ad6431f24b63043:0x88893e4a6f0e4372,10+Easey+St,+Collingwood+VIC+3066&gl=au&ei=EdjgUufsPIjikgWR64G4CQ&ved=0CCoQ8gEwAA"></a>
				</div>
			</div>

			<div class="contact-box-alt medium-4 small-12 large-4 columns">
				<div class="clickable contact-box-container contact-blog">
					<h1>blog</h1>
					<p>Keep up to date with our latest population research, Census news, population trends, insights and anecdotes here >></p>
					<a href="http://blog.id.com.au"></a>
				</div>
			</div>

			<div class="contact-box medium-4 small-12 large-4 columns">
				<div class="clickable contact-box-container contact-enews">
					<h1>e-news</h1>
					<p>Delivered to your inbox monthly - latest research, case studies, tips &amp; tricks, Census, news, product updates and much more >></p>
					<a href="/about-us/newsletter/"></a>
				</div>
			</div>

			<div class="contact-box-alt medium-4 small-12 large-4 columns">
				<div class="clickable contact-box-container contact-twitter">
					<h1>twitter</h1>
					<p>Follow us on twitter for up to the moment data release and news from the world of population statistics, data analysis, visualisation and urban planning >></p>
					<a href="http://twitter.com/dotid"></a>
				</div>
			</div>
		</div>

		<!-- CONTENT BOXES -->
		<div class="row pad sub-heroes">
			<?php while(has_sub_field("sub_hero_box")): ?>
				<!-- Generic box -->
				<?php if(get_row_layout() == "generic_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
			 		<div class="sub-hero home-box medium-4 small-12 large-4 columns">
						<div class="generic <?php if (get_sub_field('is_page_link')) echo 'clickable' ?>" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div><?php the_sub_field("content"); ?></div>
							<?php if (get_sub_field('is_page_link')):?>
								<a href="<?php echo get_sub_field('page_link') ?>"></a>
							<?php endif; ?>
				 		</div>
					 </div>
				<!-- Blogroll box -->
				<?php elseif(get_row_layout() == "blogroll_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="blogroll" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="blog-feed"></div>
							<div>
								<h6 class="more-blog-posts"><a href="http://blog.id.com.au">see more blog posts</a></h6>
							</div>
				 		</div>
					 </div>
				<!-- Video box -->	 
				<?php elseif(get_row_layout() == "video_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="video" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="playVideo">
								<a href="<?php echo get_sub_field('youtube_video') ?>"><img class="image" src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt=""></a>
							</div>
				 		</div>
					 </div>
				<!-- Subscribe box -->
				<?php elseif(get_row_layout() == "subscribe_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="subscribe" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="subscribe-form">
								<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>

								<script>
								  hbspt.forms.create({ 
								    portalId: '320463',
								    formId: '95441755-778f-41b8-b170-c4551c7b4396',
								    formData: {
							            cssClass: ''
							        },
							        validationOptions : {
							        	position : 'center left',
							        	messageClass : 'err'
							        },
							        onBeforeValidationInit:function(form){
							        	jQuery(form).validate();
							        }
								  });
								</script>
							</div>
				 		</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</div>



	<?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>