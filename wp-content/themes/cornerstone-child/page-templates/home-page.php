<?php
/**
 * Template Name: Home page
 *
 * Description: The home page, no sidebar.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

		get_header( 'home' ); ?>

		<div class="row pad-double header">
			<div class="logo-home"></div>
			<!-- AddThis Button BEGIN -->
		    <div id="addThisBtns" class="addthis_toolbox addthis_default_style idc-share idc-share-small">
		        <a class="idc-pagePdf" href="http://profile.id.com.au/albany/Reporter/Create?id=home&amp;format=1"></a>
		        <a class="addthis_button_google_plusone_share at300b" style="cursor: pointer" href="http://www.addthis.com/bookmark.php?v=300&amp;winname=addthis&amp;pub=ra-503ae79e5abcd091&amp;source=tbx-300&amp;lng=en-US&amp;s=google_plusone_share&amp;url=http%3A%2F%2Fprofile.id.com.au%2Falbany&amp;title=Community%20profile%20%7C%20City%20of%20Albany%20%7C%20profile.id&amp;ate=AT-ra-503ae79e5abcd091/-/-/52d5e5abcf08cb09/2&amp;frommenu=1&amp;uid=52d5e5abdcaf3893&amp;ct=1&amp;pre=http%3A%2F%2Fprofile.id.com.au%2F&amp;tt=0&amp;captcha_provider=nucaptcha" target="_blank" title="Google+"><span class="at16nc at300bs at15nc at15t_google_plusone_share at16t_google_plusone_share"><span class="at_a11y">Share on google_plusone_share</span></span></a>
		        <a class="addthis_button_twitter at300b" style="cursor: pointer" title="Tweet" href="#"><span class="at16nc at300bs at15nc at15t_twitter at16t_twitter"><span class="at_a11y">Share on twitter</span></span></a>
		        <a class="addthis_button_facebook at300b" style="cursor: pointer" title="Facebook" href="#"><span class="at16nc at300bs at15nc at15t_facebook at16t_facebook"><span class="at_a11y">Share on facebook</span></span></a>
		        <a class="addthis_button_linkedin at300b" style="cursor: pointer" href="http://www.addthis.com/bookmark.php?v=300&amp;winname=addthis&amp;pub=ra-503ae79e5abcd091&amp;source=tbx-300&amp;lng=en-US&amp;s=linkedin&amp;url=http%3A%2F%2Fprofile.id.com.au%2Falbany&amp;title=Community%20profile%20%7C%20City%20of%20Albany%20%7C%20profile.id&amp;ate=AT-ra-503ae79e5abcd091/-/-/52d5e5abcf08cb09/3&amp;frommenu=1&amp;uid=52d5e5abe4ad1c11&amp;ct=1&amp;pre=http%3A%2F%2Fprofile.id.com.au%2F&amp;tt=0&amp;captcha_provider=nucaptcha" target="_blank" title="LinkedIn"><span class="at16nc at300bs at15nc at15t_linkedin at16t_linkedin"><span class="at_a11y">Share on linkedin</span></span></a>
		    	<div class="atclear"></div>
		    </div>
		    <script src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-503ae79e5abcd091" async=""></script>
		    <!-- AddThis Button END -->
		</div>

		<div class="pad-double row use-cases">
			<?php if(get_field('use_cases')): ?>
				<!-- Place somewhere in the <body> of your page -->
				<div class="flexslider">
				  <ul class="slides">
			      	<?php while(has_sub_field('use_cases')): ?>
						<?php if(get_sub_field('featured')): ?>
							<li class="clickable">
								<img class=" image" src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" width="960" height="350">
								<div class="overlay">
									<h1 class="ZurichBT-RomanCondensed uc-name"><?php the_sub_field('title'); ?></h1>
									<div class="uc-description"><?php the_sub_field('description'); ?></div>
								</div> 
								<?php if(get_sub_field('link_type') != "No link"): ?>
									<a href="<?php echo get_sub_field('link_type') == "Page link" ? get_sub_field('page_link') : get_sub_field('external_link') ?>"></a>
								<?php endif; ?>
							</li>
						<?php endif; ?>
					<?php endwhile ?>
				  </ul>
				</div>
			<?php endif; ?>
		</div>

		<!-- HEROeS -->
		<div class="row heros">
			<?php while(has_sub_field("hero_box")): ?>
				<?php if(get_row_layout() == "hero_box" && get_sub_field('featured')): // layout: Content ?>
			 		<div class="hero-products small-12 large-6 medium-6 columns">
				 		<div class="clickable">
				 			<h1 class="ZurichBT-RomanCondensed"><?php the_sub_field("hero_title"); ?></h1>
							<h2><?php the_sub_field("hero_subtitle"); ?></h2>
							<p><span class="p-arrow-grey"></span><?php the_sub_field("hero_description"); ?></p>
							<a href="<?php echo get_sub_field('link') ?>"></a>
				 		</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>

		<!-- CONTENT BOXES -->
		<div class="row pad sub-heroes">
			<?php while(has_sub_field("sub_hero_box")): ?>
				<!-- Generic box -->
				<?php if(get_row_layout() == "generic_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
			 		<div class="sub-hero home-box medium-4 small-12 large-4 columns">
						<div class="generic <?php if (get_sub_field('is_page_link')) echo 'clickable' ?>" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="generic-box"><?php the_sub_field("content"); ?></div>
							<?php if (get_sub_field('is_page_link')):?>
								<a href="<?php echo get_sub_field('page_link') ?>"></a>
							<?php endif; ?>
				 		</div>
					 </div>
				<!-- Blogroll box -->
				<?php elseif(get_row_layout() == "blogroll_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="blogroll" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="blog-feed"></div>
							<div>
								<h6 class="more-blog-posts"><a href="http://blog.id.com.au">see more blog posts</a></h6>
							</div>
				 		</div>
					 </div>
				<!-- Video box -->	 
				<?php elseif(get_row_layout() == "video_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="video" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="playVideo">
								<a href="<?php echo get_sub_field('youtube_video') ?>"><img class="image" src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt=""></a>
							</div>
				 		</div>
					 </div>
				<!-- Subscribe box -->
				<?php elseif(get_row_layout() == "subscribe_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="subscribe" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="subscribe-form">
								<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>

								<script>
								  hbspt.forms.create({ 
								    portalId: '320463',
								    formId: '95441755-778f-41b8-b170-c4551c7b4396',
								    formData: {
							            cssClass: ''
							        },
							        validationOptions : {
							        	position : 'center left',
							        	messageClass : 'err'
							        },
							        onBeforeValidationInit:function(form){
							        	jQuery(form).validate();
							        }
								  });
								</script>
							</div>
				 		</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>

		<div class="row home-links pad-double">
			<div class="footer-box inverted medium-4 small-6 large-2 columns">
				<div class="address">
					<h5>call us </h5>
					<p>+61 3 9417 2205<br/>
						NZ Freecall:<br/>
						0800 955 481
					</p>
				</div>
			</div>
			<div class="footer-box medium-4 small-6 large-2 columns">
				<div>
					<h5>contact us </h5>
					<div class="vcard">
						<p class="adr">
							<span class="email"><a style="display:block; font-size:1.5em; color: #54514e; margin-bottom:-4px;" href="mailto:info@id.com.au">info@id.com.au</a><span><br>
							<span class="street-address">10 Easey street, PO Box 1689</span><br>
							<span class="region">Collingwood VIC</span>
							<span class="postal-code">3066</span><br>
							<span class="country-name">Australia</span>
						</p>
					</div>
				</div>
			</div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="/about-us/newsletter/"></a><h5>sign up <br/><span>to our <br/> newsletter</span></h5></div></div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="http://blog.id.com.au"></a><h5>follow <br/><span>our <br/> blog</span></h5></div></div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="http://www.youtube.com/user/informeddecisions"></a><h5>access <br/><span>training <br /> videos</span></h5></div></div>
			<div class="footer-box medium-4 small-6 large-2 columns"><div class="clickable"><a href="/public-resources/demographic-resource-centre/"></a><h5>access <br/><span>demographic <br /> resource <br /> center</span> </h5></div></div>
		</div>
		<div class="row pad-double">
			<div class="footer-disclaimer">
				<p>	DISCLAIMER: While all due care has been taken to ensure that the content of this website is accurate and current, there may be errors or omissions in it and no legal responsibility is accepted for the information and opinions in this website
				</p>
			</div>
		</div>
		<?php wp_footer(); ?>
		</div>
		<!-- Start of Async HubSpot Analytics Code -->
			<script type="text/javascript">
			    (function(d,s,i,r) {
			        if (d.getElementById(i)){return;}
			        var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
			        n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/320463.js';
			        e.parentNode.insertBefore(n, e);
			    })(document,"script","hs-analytics",300000);
			</script>
		<!-- End of Async HubSpot Analytics Code -->
	</body>
</html>