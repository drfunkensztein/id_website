<?php
/**
 * Template Name: Local government products page
 *
 * Description: The local government product page.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header(); ?>
<div class="row pad-double">
	
	<div id="content" class="large-12 columns" role="main">
		<div class="row pad-double">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>
			<?php endwhile; ?>
		</div>

		<div class="row pad-double">
			<?php if(get_field('panel')): ?>
				<section class="resource-panels">
					<?php while(has_sub_field('panel')): ?>
							<?php if(get_sub_field('featured')): ?>
								<article class="resource-panel small-12 medium-12 large-12 columns">
									<div class="container drop-shadow clearfix">
										<div class="clearfix panel-info <?php if (get_sub_field('panel_link')) echo 'clickable' ?>">
											<img class="panel-img" src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="">
											<div>
												<span><?php the_sub_field('subtitle'); ?></span>
												<h1 class="ZurichBT-RomanCondensed"><?php the_sub_field('title'); ?></h1>
											</div>
											<?php if (get_sub_field('panel_link')):?>
												<a href="<?php echo get_sub_field('panel_link') ?>"></a>
											<?php endif; ?>
										</div>
										
										<ul class="panel-links <?php echo get_sub_field('color_scheme'); ?>">
											<?php while(has_sub_field('links')): ?>
												<?php 
													  $linktype = get_sub_field('link_type');
													  browser()->log($linktype);
												?>
												<li> 
													<?php if (get_sub_field('link_type') == "No link"):?>
														<?php echo get_sub_field('link_description'); ?>
													<?php else : ?>
														<a href="<?php $link = get_sub_field('page_link') == "Page" ?  get_sub_field('page_link') : get_sub_field('external_link'); echo $link ?>"><?php echo get_sub_field('link_description'); ?></a> 
													<?php endif; ?>
												</li>
											<?php endwhile ?>
										</ul>

									</div>
								</article>
							<?php endif; ?>
					<?php endwhile ?>
				</section>
			<?php endif; ?>
		</div>
		
		<!--<div class="pad row">
			 <div class="large-9 medium-9">

				<div class="clickable localgov-product prod-community medium-6 small-12 large-6 columns">
					<div class="header">
						<h1>community profile</h1>
						<h1 class="subheader"><span class="p-arrow-white"></span>profile.id</h1>
					</div>
					<div class="container clearfix">
						<p><strong>understand your <br>community</strong><span> >></span></p>
					</div>
					<a href="/services/local-government-products/profile-id/"></a>
				</div>

				<div class="clickable localgov-product prod-forecast medium-6 small-12 large-6 columns">
					<div class="header">
						<h1>population forecasts</h1>
						<h1 class="subheader"><span class="p-arrow-white"></span>forecast.id</h1>
					</div>
					<div class="container clearfix">
						<p><strong>Plan for <br> the future</strong><span> >></span></p>
					</div>
					<a href="/services/local-government-products/forecast-id/"></a>
				</div>
				
				<div class="clickable localgov-product prod-atlases medium-6 small-12 large-6 columns">
					<div class="header">
						<h1>social atlases </h1>
						<h1 class="subheader"><span class="p-arrow-white"></span>atlas.id</h1>
					</div>
					<div class="container clearfix">
						<p><strong>Allocate <br> your resources</strong>	</p>
					</div>
					<a href="/services/local-government-products/atlas-id/"></a>
				</div>

				<div class="clickable localgov-product prod-economy medium-6 small-12 large-6 columns">
					<div class="header">
						<h1>economic profiles</h1>
						<h1 class="subheader"><span class="p-arrow-white"></span>economy.id</h1>
					</div>
					<div class="container clearfix">
						<p><strong>Understand your workers <br> and economy</strong></p>
					</div>
					<a href="/services/local-government-products/economy-id/"></a>
				</div>


				<div class="clickable localgov-product prod-housing medium-6 small-12 large-6 columns">
					<div class="header">
						<h1>housing analysis</h1>
						<h1 class="subheader"><span class="p-arrow-white"></span>housing.id</h1>
					</div>
					<div class="container clearfix">
						<p><strong>Inform you <br> housing policy</strong><span>>></span></p>
					</div>
					<a href="/services/local-government-products/housing-id/"></a>
				</div>

				<div class="clickable localgov-product prod-consulting medium-6 small-12 large-6 columns">
					<div class="header">
						<h1>demographic expertise</h1>
						<h1 class="subheader"><span class="p-arrow-white"></span>consulting services</h1>
					</div>
					<div class="container clearfix">
						<p><strong>Build evidence-based <br> policy</strong><span>>></span></p>
					</div>
					<a href="/services/consulting-services/"></a>
				</div>

			</div> 
		</div>-->

		<!-- CONTENT BOXES -->
		<div class="row pad sub-heroes">
			<?php while(has_sub_field("sub_hero_box")): ?>
				<!-- Generic box -->
				<?php if(get_row_layout() == "generic_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
			 		<div class="sub-hero home-box medium-4 small-12 large-4 columns">
						<div class="generic <?php if (get_sub_field('is_page_link')) echo 'clickable' ?>" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div><?php the_sub_field("content"); ?></div>
							<?php if (get_sub_field('is_page_link')):?>
								<a href="<?php echo get_sub_field('page_link') ?>"></a>
							<?php endif; ?>
				 		</div>
					 </div>
				<!-- Blogroll box -->
				<?php elseif(get_row_layout() == "blogroll_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="blogroll" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="blog-feed"></div>
							<div>
								<h6 class="more-blog-posts"><a href="http://blog.id.com.au">see more blog posts</a></h6>
							</div>
				 		</div>
					 </div>
				<!-- Video box -->	 
				<?php elseif(get_row_layout() == "video_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="video" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="playVideo">
								<a href="<?php echo get_sub_field('youtube_video') ?>"><img class="image" src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt=""></a>
							</div>
				 		</div>
					 </div>
				<!-- Subscribe box -->
				<?php elseif(get_row_layout() == "subscribe_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="subscribe" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="subscribe-form">
								<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>

								<script>
								  hbspt.forms.create({ 
								    portalId: '320463',
								    formId: '95441755-778f-41b8-b170-c4551c7b4396',
								    formData: {
							            cssClass: ''
							        },
							        validationOptions : {
							        	position : 'center left',
							        	messageClass : 'err'
							        },
							        onBeforeValidationInit:function(form){
							        	jQuery(form).validate();
							        }
								  });
								</script>
							</div>
				 		</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</div>
	<?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>