<?php
/**
 * Template Name: Placemaker page
 *
 * Description: A static page for placemaker.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header('placemaker'); ?>
<div class="placemaker-header">
	<div class="leftwhite"></div>
	<div class="id-logo placemaker">
		<a href="/"></a>
	</div>
</div>
<div class="pm-section section pm-banner">
	<div class="container">
		<div class="pm-column full">
			<h1>invest in the right place<br /> at the right time</h1>
		</div>
	</div>
</div>
<div class="pm-section section pm-intro full-title">
	<div class="container title-container">
		<div class="pm-column full">
			<h2>Introducing Placemaker</h2>
		</div>
	</div>
</div>
<div class="pm-section section pm-menu">
	<div class="container">
		<div class="pm-column fivetwelfths">
			<h3>what is .id placemaker?</h3>
			<p>As population experts, it’s our business to forecast the future of our cities, towns and regions.</p>
			<p>Now with .id placemaker, your business can access this knowledge too.</p>
			<p>Placemaker combines our population insights with your business data, into an easy to use yet powerful cloud-based platform designed for decision makers.</p>
			<p>All of this is set up and managed for you. We select and prepare the data, customise and manage placemaker in the cloud so that you can focus on discovering what you need to know.</p>
			<p>The ability to explore, discover and make decisions is in your hands.</p>
		</div>
	</div>
	<div class="menu">
		<a href="#" data-section="pm-location" class="anchor location" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">locational intelligence</span>
				<span class="description">Learn how the unique combination of our population insights and your business data give you the edge in a spatial world.</span>
			</span>
		</a>
		<a href="#" data-section="pm-platform" class="anchor platform" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">the platform</span>
				<span class="description">Seeing the future has never been simpler in this powerful spatial platform deployed and managed by us. No need to learn how to use software – spend your time gaining insights.</span>
			</span>
		</a>
		<a href="#" data-section="pm-case-studies" class="anchor who" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">who&rsquo;s using placemaker?</span>
				<span class="description">Australia is changing rapidly. Learn how others are already using placemaker to build the future of their organisations.</span>
			</span>
		</a>
		<a href="#" data-section="pm-contact" class="anchor contact" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">get in touch</span>
				<span class="description">Tell us about the questions your business needs to answer.</span>
			</span>
		</a>
	</div>
</div>
<div class="pm-section section pm-location">
	<div class="container title-container">
		<div class="pm-column full">
			<h2>Locational Intelligence</h2>
		</div>
	</div>
	<div class="animation-container">
		<div class="animation animation-waiting">
			<div class="text">
				<div class="text text-1">
					<span class="line"></span>
					<p><strong>Your business data<br />
					<em>Supply</em></strong><br />
					e.g facility locations,<br />
					existing customers, <br />
					catchments, <br />
					penetration rates, <br />
					competitor information</p>
				</div>
				<div class="text text-2"><span class="line"></span>
					<p><strong>.id’s population<br />
					insights<br />
					<em>Demand</em></strong><br />
					Where and when <br />
					target populations<br />
					are changing</p></div>
				<div class="text text-3"><span class="line"></span><p><strong>Base maps</strong><br />
					Quickly see<br />
					relationships by <br />
					layering data <br />
					spatially
					</p></div>
			</div>
			<div class="layers">
				<div class="layer-1 layer"></div>
				<div class="layer-2 layer"></div>
				<div class="layer-3 layer"></div>
			</div>
		</div>
		<div class="container">
			<div class="pm-column third">
				<h3>population insights<br />
				+ your data<br />
				= locational intelligence</h3>
				<p>By mapping your business data with .id’s census and forecasting data, placemaker gives you unprecedented levels of insight into how populations are changing and shows you when and where to respond.</p>
				<p>We call this Locational Intelligence.</p>
				<p>It’s not about having a lot of data, it’s about the right data, presented in the right way to answer your questions. </p>
				<p>As the experts, we consult and then extract, prepare and populate placemaker with the data that you need to make decisions.</p>
				<p><a href="http://hub.am/1fZXLUY">Learn more</a> about our population insights.</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="pm-column third">
			<div class="pull-out-box">
				<div class="box-inner">
					<p><strong>Data from the experts</strong><br />
					.id has the largest population forecast team in Australia. We have developed forecasts for any custom geographic unit. See population change across cities as never before – from cities to neighbourhoods to catchments. <br />
					<a href="/services/population-forecasting-safi/">Learn more</a></p>
				</div>
			</div>
		</div>
		<div class="pm-column third">
			<div class="pull-out-box vid-box">
				<a href="http://www.youtube.com/watch?v=cxA9SJmydeI" class="vid"><img src="../../wp-content/themes/cornerstone-child/img/placemaker/video.jpg" alt=""/></a>
				<div class="box-inner">
					<p><strong>Video: How can demographics help your business?</strong></p>
				</div>
			</div>
		</div>
	</div>
	<a href="#" data-section="pm-intro" class="btt anchor button white">Back to top</a>
</div>
<div class="pm-section section pm-quote ivan-quote">
	<div class="container">
		<div class="quote">
			<h3><span class="quote-left">&ldquo;</span>For our clients, .id placemaker is a partnership where they gain more than just a software tool, but a new set of practices, new ways of looking at data, and most importantly, new ways of seeing the world.<span class="quote-right">&rdquo;</span></h3>
			<h5><strong>Ivan Motley</strong><br />
			Founder<br />
			.id</h5>
		</div>
	</div>
</div>
<div class="pm-section section pm-platform">
	<img src="../../wp-content/themes/cornerstone-child/img/placemaker/mac.png" alt="" class="platform-feature"/>
	<div class="container title-container">
		<div class="pm-column full">
			<h2>The Platform</h2>
		</div>
	</div>
	<div class="container">
		<div class="pm-column fivetwelfths">
			<h3>your virtual analyst</h3>
			<p>Designed for simplicity and ease of use, .id placemaker&rsquo;s web-based mapping platform puts the power of exploration and decision making back into the hands of the people who need it most.</p>
			<p>Decision makers.</p>
			<p>Tailored specifically for the needs of your business, and updated as your requirements evolve, .id placemaker removes the technical burdens and complexity. It is designed to provide answers quickly and intuitively.</p>
			<p>Placemaker can be set up for executive use or to compliment your existing team of analysts.</p>
		</div>
	</div>
	<div class="container platform-boxes">
		<div class="pm-column third">
			<div class="icon-box">
				<img src="../../wp-content/themes/cornerstone-child/img/placemaker/explore.png" alt=""/>
				<h4>explore &amp; discover</h4>
				<p>Placemaker&rsquo;s power isn&rsquo;t just in seeing the answers, but the exploration to arrive at them.</p>
				<p>Placemaker lets you explore new territories, ask the “what if” questions and discover opportunities that you never knew existed.</p>
			</div>
		</div>
		<div class="pm-column third">
			<div class="icon-box">
				<img src="../../wp-content/themes/cornerstone-child/img/placemaker/macro.png" alt=""/>
				<h4>macro to micro</h4>
				<p>Placemaker allows you to make location decisions about a specific place, safe in the knowledge that you have explored the bigger picture. Zoom in and out from macro to micro as you explore changes across your territory.</p>
			</div>
		</div>
		<div class="pm-column third">
			<div class="icon-box">
				<img src="../../wp-content/themes/cornerstone-child/img/placemaker/evidence.png" alt=""/>
				<h4>present evidence visually</h4>
				<p>Placemaker&rsquo;s easy point and click interface means that not only do you see the story, but you can present it to others in a compelling way.</p>
				<p>Use it to visually present the evidence, create engaging discussion and buy-in to decisions.</p>
			</div>
		</div>
	</div>
	<a href="#" data-section="pm-intro" class="btt anchor button">Back to top</a>
</div>
<div class="pm-section section pm-quote patrick-quote">
	<div class="container">
		<div class="quote">
			<h3>
				<span class="quote-left">&ldquo;</span>Placemaker assists us to make confident decisions about when and where to invest for future school development in Victoria.<span class="quote-right">&rdquo;</span>
			</h3>
			<h5><strong>Patrick Love</strong><br/>
				Manager Planning<br/>
				Catholic Education Office, Vic</h5>
		</div>
	</div>
</div>
<div class="pm-case-studies pm-section">
	<div class="container title-container">
		<div class="pm-column full">
			<h2>Who's Using Placemaker</h2>
		</div>
	</div>
	<div class="container case-studies-container">
		
<div class="pm-column third">
	<span class="case-study">
		<img src="../../wp-content/themes/cornerstone-child/img/placemaker/red-cross.gif" alt="red cross" width="297" height="146"/>
		<span class="title">Australian Red Cross Blood Service</span>
		<span class="description">The Australian Red Cross Blood Service (ARCBS) is responsible for meeting the nation&rsquo;s need for blood products. By spatially presenting data that combines relevant demographic patterns and trends across Australia with ARCBS donor data, .id placemaker provides strategic insights into the distribution of current donors and assists ARCBS prioritise its efforts to attract new donors.</span>
			</span>
</div>
<div class="pm-column twothirds">
	<span class="case-study">
		<img src="../../wp-content/themes/cornerstone-child/img/placemaker/afl.gif" alt="Australian Football League" width="232" height="174"/>
		<span class="title">Australian Football League</span>
		<span class="description">The AFL has ambitious membership targets. .id placemaker provides each club with strategic insights into the distribution of current members and where else they might effectively attract new members &ndash; now and in the future.</span>
			</span>
</div>
<div class="pm-column third">
	<span class="case-study">
		<img src="../../wp-content/themes/cornerstone-child/img/placemaker/ceo.gif" alt="Catholic Education Office" width="297" height="158"/>
		<span class="title">Catholic Education Office</span>
		<span class="description">The Catholic Education Office (CEO) engaged .id to assist them in matching their school services to their potential student base across Victoria.<br />
<br />
Placemaker enables them to make evidence-based decisions about where and when to open and close schools; what capacity those schools will need to provide for at what time; and crucially, where and when they need to invest in land and other assets for future school development.</span>
			</span>
</div>
<div class="pm-column third">
	<span class="case-study">
		<img src="../../wp-content/themes/cornerstone-child/img/placemaker/stockland.gif" alt="Stockland" width="297" height="158"/>
		<span class="title">Stockland</span>
		<span class="description">Stockland works with .id to identity when and where demand for its residential estates and retirement villages will be strongest in the future. As Australia\'s baby boomers are moving into retirement age, .id can show Stockland how retirees will be distributed across our major cities over the next 25 years, so that the company can plan its acquisitions and product mix.</span>
			</span>
</div>
<div class="pm-column third">
	<span class="case-study">
		<img src="../../wp-content/themes/cornerstone-child/img/placemaker/western-water.gif" alt="Western Water" width="297" height="133"/>
		<span class="title">Western Water</span>
		<span class="description">Western Water is a water utility company supplying water to the western part of Melbourne. Melbourne\'s west is experiencing significant population growth, with some of the fastest growing suburbs in the country. .id helps Western Water forecast future demand for water by location - even in places where farmland still prevails today, but that will be the suburbs of the future.</span>
			</span>
</div>
<div class="pm-column third">
	<span class="case-study">
		<img src="../../wp-content/themes/cornerstone-child/img/placemaker/gmct.gif" alt="Melbourne Cemeteries Trust" width="297" height="132"/>
		<span class="title">Melbourne Cemeteries Trust</span>
		<span class="description">When you are population forecasters, it means you spend quite a bit of time forecasting deaths too. This information, when presented spatially, and combined with other data such as people\'s religion and ethnicity, assists the GMCT to plan its service mix in different cemeteries and memorial parks across Melbourne - as well as plan for future needs as the population grows.</span>
			</span>
</div>	</div>
	<a href="#" data-section="pm-intro" class="btt anchor button">Back to top</a>
</div>
<div class="pm-contact pm-section">
	<div class="container">
		<div class="pm-column full">
			<div class="contact-left">
				<h3>get in touch</h3>
				<p>Talk to us about how we can tailor .id placemaker and unlock the power of locational intelligence for your business.</p>
				<p class="contact-call"><strong>call</strong><br />
				+61 3 9417 2205<br />
				NZ Freecall: 0800 955 481</p>
				<p class="contact-email"><strong>email</strong><br />
				<a href="mailto:info@id.com.au">info@id.com.au</a></p>
			</div>
			<div class="contact-right">
				<h3>or we can contact you</h3>
				<div class="standard-form">

					<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>
					<script>
					  hbspt.forms.create({ 
					    portalId: '320463',
					    formId: '97f6a013-c92f-48a7-a4b2-580bc4c46116',
					    css : '',
					    validationOptions : {
				        	position : 'center left',
				        	messageClass : 'err'
				        },
				        onBeforeValidationInit:function(form){
				        	jQuery(form).validate();
				        }
					  });
					</script>



					<!-- <form action="id-placemaker#contact-form" method="post" id="contact-form">
						<input type="hidden" name="flagSubmitted" value="1"/>
						<input type="text" placeholder="your name" name="firstName" required="true" value=""/>
						<input type="text" placeholder="organisation" name="company" required="true" value=""/>
						<input type="email" placeholder="your email" name="emailAddress" required="true" value=""/>
						<input type="phone" placeholder="daytime phone" name="phoneNumber" required="true" value=""/>
						<input type="submit" value="submit" name="contactSubmit" class="submit" />
					</form> -->
				</div>
			</div>
		</div>
	</div>
</div>
<footer class="main-footer row pad-double">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Sidebar')) : ?>
		<?php endif; ?>
		<?php if ( has_nav_menu( 'footer-menu' ) ) {
			wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'inline-list', 'container' => 'nav', 'container_class' => 'large-12 columns' ) );
		} ?>

	</footer>
	<div class="row ">
		<div class="footer-disclaimer">
			<p>	DISCLAIMER: While all due care has been taken to ensure that the content of this website is accurate and current, there may be errors or omissions in it and no legal responsibility is accepted for the information and opinions in this website
				</p>
		</div>
	</div>
	<?php wp_footer(); ?>
	

</body>
</html>