<?php
/**
 * Template Name: Public resources page
 *
 * Description: A public resources page (community profiles, population forecasts, economic profiles, social atlases)
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header( 'publicresources' ); ?>
<div class="row pad-double">
	
	<div id="content" class="large-12 columns" role="main">
		<div class="row pad-double" style="display:none">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>
			<?php endwhile; ?>
		</div>
		<div class="row pad-double clients">
			<div  class="large-5 medium-5 small-12 column">
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<?php the_content(); ?>
						</div>
						<footer class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
						</footer>
					</article>
				<?php endwhile; ?>			
			</div>
		   <div class="large-7 medium-7 small-12 column">
			<div class="column no-padding filter-container ">
		    	<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion active">ALL</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">ACT</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">NSW</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">NT</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">QLD</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">SA</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">TAS</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">VIC</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">WA</button>
				<button class="column triangle-isosceles tiny custom-large-1 small-6 custom-small-1 custom-medium-1 clientFilterRegion">NZ</button>
		    </div>
		    
			<div class="column no-padding client-list-container" >
				<!-- <ul class="client-list">
					<div class="clientListContainer hidden">
						<li class="{highlightClass}">
		      				<a class="client-col" href="{<?php echo get_field('clientproduct'); ?>Url}" target="_blank">{areaName}</a>
	    				</li>
					</div>
				</ul>	 -->

				<table class="scrollbox client-list">
					<tbody class="clientListContainer hidden">
						<tr class="{highlightClass}">
		      				<td class="client-col"><a href="{<?php echo get_field('clientproduct'); ?>Url}" target="_blank">{areaName}</a></td>
	    				</tr>
					</tbody>
				</table>	
			</div>
		    <div class="loader hidden">
		    	<img src="<?php echo get_stylesheet_directory_uri() . '/img/ajax-loader.gif'?>" alt="loading..."/>
		    </div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var clientProduct = '<?php echo get_field('clientproduct'); ?>';
	</script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . '/js/clientlist.js' ?> "></script>
	
	<?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>