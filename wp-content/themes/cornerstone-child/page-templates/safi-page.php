<?php
/**
 * Template Name: SAFi page
 *
 * Description: A static page for SAFi.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header('placemaker'); ?>

<div class="placemaker-header">
	<div class="leftwhite"></div>
	<div class="id-logo safi">
		<a href="/"></a>
	</div>
</div>

<div class="safi-section section safi-banner">
	<div class="container">
		<div class="safi-column full">
			<h1>What's the future of<br /> Australia's population?<br /> SAFi knows</h1>
		</div>
	</div>
</div>

<div class="safi-section section safi-intro full-title">
	<div class="container title-container">
		<div class="safi-column full">
			<h2>Introducing SAFi</h2>
		</div>
	</div>
</div>

<div class="safi-section section safi-menu">
	<div class="container">
		<div class="safi-column fivetwelfths">
			<h3>What is SAFi?</h3>
			<p>SAFi, also known as Small Area Forecast Information lets you achieve unprecedented insights into the future of Australia’s  populations,  with the reliability you need to make important decisions.</p>
			<p>SAFi not only allows you to see into the future, but it unlocks unprecedented levels of detail right down to the city block.</p>
			<p>This level of detail is what sets SAFi apart. With SAFi, you can take a bird’s eye view of population change across an entire city, then drill right down to a city block, allowing you to understand  crucial details of change.</p>
			<p>As population  experts, our job is to give people the tools they need to make decisions.  We’ve perfected SAFi over years of research and development, honing it into an effective and reliable tool, an essential ingredient  for any contemporary decision making process.</p>
			<p>You can incorporate SAFi into your existing process and platforms, or use .id’s placemaker mapping platform to present the forecasts spatially and layer them with your own data.</p>
		<div class="pm-column">
			<div class="pull-out-box">
				<div class="box-inner">
					<p><strong>Data from the experts</strong><br>
					In our e-book “Three Growth Markets in Australia”, we use .id population forecasts to chart the course of the Australian population profile over the next twenty years. While the baby boomer ‘bulge’ remains prominent, we identify a further two population peaks, following around 30 and 60 years after the baby boomers.<br>
					<a href="http://hub.am/1fZXLUY">Learn more</a></p>
				</div>
			</div>
		</div>
		</div>
		
	</div>
	<div class="menu">
		<a href="#" data-section="safi-insight" class="anchor location" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">population insights</span>
				<span class="description">Learn how SAFi  can provide you with a new way of understanding your business by viewing demand through a demographic lens.</span>
			</span>
		</a>
		<a href="#" data-section="safi-experts" class="anchor platform" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">population experts</span>
				<span class="description">Learn  about  the business behind SAFi and the approach we take to building sophisticated and reliable population forecasts.</span>
			</span>
		</a>
		<a href="#" data-section="safi-placemaker" class="anchor who" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">powering .id placemaker</span>
				<span class="description">You can use SAFi on its own in existing GIS platforms, or combine it with your business data into .id placemaker, a powerful, but easy to use, spatial platform for decision makers.</span>
			</span>
		</a>
		<a href="#" data-section="safi-users" class="anchor clients" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">who’s using safi?</span>
				<span class="description">Join an ever-growing list of Australian organisations who are using  SAFi as a crucial ingredient in their decision making process.</span>
			</span>
		</a>
		<a href="#" data-section="safi-contact" class="anchor contact" rel="nofollow">
			<span class="menu-item-bg"></span>
			<span class="menu-item-container">
				<span class="title">get in touch</span>
				<span class="description">Contact us to learn  more  about  how SAFi can change your business by helping you respond to change.</span>
			</span>
		</a>
	</div>
</div>

<div class="safi-gallery-container section safi-insight">

	<a class="close">&times;</a>

	<div class="ticker">
		<h3>Melbourne - population</h3>
		<div id="odometer" class="odometer">2011</div>
	</div>

	<div class="hotspots">

		<!-- pins -->
		<div data-modal="modal-1" class="pin pin-1"><span class="pin-body"></span></div>
		<div data-modal="modal-2" class="pin pin-2"><span class="pin-body"></span></div>
		<div data-modal="modal-3" class="pin pin-3"><span class="pin-body"></span></div>

		<!-- modals -->
		<div class="modal-popup modal-1">
			<span class="tip-pin1"></span>
			<div class="mini-slideshow slideshow-modal-1">
				<ul class="slides">
					<li data-year="2011"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/tarneit-2011.jpg" alt=""></li>
					<li data-year="2016"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/tarneit-2016.jpg" alt=""></li>
					<li data-year="2021"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/tarneit-2021.jpg" alt=""></li>
					<li data-year="2026"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/tarneit-2026.jpg" alt=""></li>
					<li data-year="2031"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/tarneit-2031.jpg" alt=""></li>
					<li data-year="2036"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/tarneit-2036.jpg" alt=""></li>
				</ul>
			</div>
			<h4 class="city">Tarneit <span class="odo-modal-1 odo-mini odometer">2011</span></h4>
			<p>A classic growth area on the fringe of Melbourne, Tarneit is in Melbourne’s western growth corridor. SAFi enables you to see the staging of development as farmland is converted into housing estates and time your response.</p>
		</div>

		<div class="modal-popup modal-2">
			<span class="tip-pin2"></span>
			<div class="mini-slideshow slideshow-modal-2">
				<ul class="slides">
					<li data-year="2011"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/Footscray-2011.jpg" alt=""></li>
					<li data-year="2016"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/Footscray-2016.jpg" alt=""></li>
					<li data-year="2021"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/Footscray-2021.jpg" alt=""></li>
					<li data-year="2026"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/Footscray-2026.jpg" alt=""></li>
					<li data-year="2031"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/Footscray-2031.jpg" alt=""></li>
					<li data-year="2036"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/Footscray-2036.jpg" alt=""></li>
				</ul>
			</div>
			<h4 class="city">Footscray <span class="odo-modal-2 odo-mini odometer">2011</span></h4>
			<p>In Footscray population growth is being driven by increasing land values due to accessibility to employment in central Melbourne. Supply is relatively plentiful as developers convert underutilized industrial and maritime lands into apartments. With its large retail precinct, the area also lends itself to mixed use development with apartments being built above shops.</p>
		</div>

		<div class="modal-popup modal-3">
			<span class="tip-pin3"></span>
			<div class="mini-slideshow slideshow-modal-3">
				<ul class="slides">
					<li data-year="2011"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/epping-2011.jpg" alt=""></li>
					<li data-year="2016"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/epping-2016.jpg" alt=""></li>
					<li data-year="2021"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/epping-2021.jpg" alt=""></li>
					<li data-year="2026"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/epping-2026.jpg" alt=""></li>
					<li data-year="2031"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/epping-2031.jpg" alt=""></li>
					<li data-year="2036"><img src="../../wp-content/themes/cornerstone-child/img/safi/minislideshows/epping-2036.jpg" alt=""></li>
				</ul>
			</div>
			<h4 class="city">Epping <span class="odo-modal-3 odo-mini odometer">2011</span></h4>
			<p>Epping is in Melbourne’s northern growth corridor. At the SA2 level all you can see is that it is growing, but drill down to SAFi areas and you can see the staging of new development pushing into farmland in the north creating population growth. At the same time in the southern part of Epping, the population is in decline as children of areas that were established in the 1980s-90s leave home. Age structures will vary significantly between these two part of the suburb.</p>
		</div>
	</div>

	<div class="safi-gallery-overlay"></div>
	
	<div class="slideshow">
		<div class="top-shadow"></div>
		<ul class="slides">
			<li data-year="2011"> <img src="../../wp-content/themes/cornerstone-child/img/safi/slideshow/slide-2011.jpg" alt=""></li>
			<li data-year="2016"> <img src="../../wp-content/themes/cornerstone-child/img/safi/slideshow/slide-2017.jpg" alt=""></li>
			<li data-year="2021"> <img src="../../wp-content/themes/cornerstone-child/img/safi/slideshow/slide-2024.jpg" alt=""></li>
			<li data-year="2026"> <img src="../../wp-content/themes/cornerstone-child/img/safi/slideshow/slide-2031.jpg" alt=""></li>
			<li data-year="2031"> <img src="../../wp-content/themes/cornerstone-child/img/safi/slideshow/slide-2036.jpg" alt=""></li>
		</ul>
	</div>
</div>

<div class="safi-section section safi-location safi-platform">
	
	<div class="safi-gallery-content container title-container">
		<div class="safi-column full">
			<h2>POPULATION INSIGHTS</h2>
		</div>
		<div class="no-padding column ">
			<h3 class="pad-half">See the future in sharper detail</h3>
			<div class="safi-column third">
				<div class="text-box">
					<p>With SAFi, you can see how a population will change each year from 2011-2036.</p>
				</div>
			</div>
			<div class="safi-column third">
				<div class="text-box">
					<p>See population, number of dwellings, people in each year of age and the type of households people are living in (e.g. lone person, couple without children etc).</p>
				</div>
			</div>
			<div class="safi-column third">
				<div class="text-box">
					<p>They are delivered for the smallest geographic unit (SA1 and smaller in fringe areas), allowing you to see the crucial details that would ordinarily be missed.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container platform-boxes ">
		<div class="safi-column third">
			<div class="icon-box">
				<img src="../../wp-content/themes/cornerstone-child/img/safi/explore.png" alt=""/>
				<h4>macro to micro</h4>
				<p>Get a view of macro population change across the country, then drill down to see how this change is affecting individual neighbourhoods and city blocks so you can pinpoint the right locations for your business activity.</p>
			</div>
		</div>
		<div class="safi-column third">
			<div class="icon-box">
				<img src="../../wp-content/themes/cornerstone-child/img/safi/macro.png" alt=""/>
				<h4>regularly updated</h4>
				<p>SAFi is updated on an 18 month rolling cycle, ensuring the assumptions driving the forecasts are current.</p>
			</div>
		</div>
		<div class="safi-column third">
			<div class="icon-box">
				<img src="../../wp-content/themes/cornerstone-child/img/safi/evidence.png" alt=""/>
				<h4>integrate with <br> existing platforms</h4>
				<p>SAFi can be added as a layer to your existing GIS platforms, or delivered in .id placemaker – our spatial platform for location analysis.</p>
			</div>
		</div>
	</div>
	<a href="#" data-section="safi-intro" class="btt anchor button white">Back to top</a>
</div>

<div class="safi-section section safi-population-experts safi-experts">
	<div class="container title-container">
		<div class="safi-column full">
			<h2>POPULATION EXPERTS</h2>
		</div>
	</div>
	<div class="container">
		<div class="safi-column fivetwelfths">
			<h3>Reliable forecasting from the population experts</h3>
			<p>As population experts, our job is tell the stories about the past, present and future of populations and help organisations respond to these changes.</p>
			<p>As you can imagine, creating forecasts with the scale and detail of SAFi is no small feat. Over our 16 years, we’ve developed methods of building and refining our forecasts to ensure organisations not only get the evidence they need to make decisions, but the best possible reliability.</p>
			<p>Forecasts are only as good as the underlying assumptions that drive the model. .id has the largest population forecast team in Australia. These resources, combined with our strong relationship with local government and the development industry, give us unprecedented insight into local drivers of population change.</p>
		</div>
	</div>
	<a href="#" data-section="safi-intro" class="btt anchor button white">Back to top</a>
</div>

<div class="safi-section section ivan-quote">
	<div class="container">
		<div class="quote">
			<h3>
				<span class="quote-left quote-blue">&ldquo;</span>Forecasters have deep knowledge about how communities are changing. We call this looking at places through a demographic lens – and it can provide organisations with valuable insights for the successful operation of their business<span class="quote-right quote-blue">&rdquo;</span>
			</h3>
			<h5><strong>Ivan Motley</strong><br/>Founder<br/>.id</h5>
		</div>
	</div>
</div>

<div class="safi-section section safi-placemaker">
	<img src="../../wp-content/themes/cornerstone-child/img/placemaker/mac.png" alt="" class="platform-feature"/>
	<div class="container title-container">
		<div class="safi-column full">
			<h2>POWERING .ID PLACEMAKER</h2>
		</div>
	</div>
	<div class="container">
		<div class="safi-column fivetwelfths">
			<h3>The insights that fuel .id placemaker</h3>
			<p>Don’t have an analysis team? .id placemaker combines SAFi’s population insights with your business data into a cloud-based platform that will transform the way that you make location-based decisions.</p>
			<p>See the future story of places told before your eyes through .id place maker’s map-based visualisations.</p>
			<p>Use it to explore, compare, drill down, report and present the knowledge that will inform decision making.</p>
			<a class="fivetwelfths learn-more" href="/services/location-analysis-placemaker/">Learn more about .id placemaker</a>
		</div>
	</div>
	<a href="#" data-section="safi-intro" class="btt anchor button">Back to top</a>
</div>

<div class="safi-section section safi-quote patrick-quote">
	<div class="container">
		<div class="quote">
			<h3>
				<span class="quote-left">&ldquo;</span>Working with the .id team’s population experts introduced us to new ways of thinking about our business, and the hard data to make confident investment decisions<span class="quote-right">&rdquo;</span>
			</h3>
			<h5><strong>Wendy Mackay</strong><br/>
				General Manager, Strategy, Strategic Risk & Research<br/>
				Stockland</h5>
		</div>
	</div>
</div>

<div class="safi-case-studies safi-section safi-users">

	<div class="container title-container">
		<div class="safi-column full">
			<h2>Who's Using SAFi</h2>
		</div>
	</div>

	<div class="container case-studies-container">

		<div class="safi-column half">
			<ul>
				<li>Coles</li>
				<li>Stockland</li>
				<li>Catholic Education Office</li>
				<li>Department of Education (NSW)</li>
				<li>Australian Football League</li>
				<li>Richmond Football Club</li>
				<li>Australian Red Cross Blood Service</li>
				<li>Greater Melbourne Cemeteries Trust</li>
			</ul>
		</div>

		<div class="safi-column half">
			<ul>
				<li>NSW Electoral Commission</li>
				<li>Victorian Electoral Commission</li>
				<li>Western Water</li>
				<li>One Steel</li>
				<li>Point Project Management</li>
				<li>Hilvert Corporate Strategy & Finance</li>
				<li>Mount St Benedict College</li>
				<li>Queensland Investment Corporation</li>
			</ul>
		</div>
	</div>
	<a href="#" data-section="safi-intro" class="btt anchor button">Back to top</a>
</div>

<div class="safi-contact safi-section">
	<div class="container">
		<div class="safi-column full">
			<div class="contact-left">
				<h3>get in touch</h3>
				<p>Talk to us about how we can tailor SAFi and unlock the power of locational intelligence for your business.
</p>
				<p class="contact-call"><strong>call</strong><br />
				+61 3 9417 2205<br />
				NZ Freecall: 0800 955 481</p>
				<p class="contact-email"><strong>email</strong><br />
				<a href="mailto:info@id.com.au">info@id.com.au</a></p>
			</div>
			<div class="contact-right">
				<h3>or we can contact you</h3>
				<div class="standard-form">

					<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>
					<script>
					  hbspt.forms.create({ 
					    portalId: '320463',
					    formId: '97f6a013-c92f-48a7-a4b2-580bc4c46116',
					    css : '',
					    validationOptions : {
				        	position : 'center left',
				        	messageClass : 'err'
				        },
				        onBeforeValidationInit:function(form){
				        	jQuery(form).validate();
				        }
					  });
					</script>
				</div>
			</div>
		</div>
	</div>
</div>

<footer class="main-footer row pad-double">
		<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Sidebar')) : ?>
		<?php endif; ?>
		<?php if ( has_nav_menu( 'footer-menu' ) ) {
			wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'inline-list', 'container' => 'nav', 'container_class' => 'large-12 columns' ) );
		} ?>
</footer>

<div class="row ">
	<div class="footer-disclaimer">
		<p>	DISCLAIMER: While all due care has been taken to ensure that the content of this website is accurate and current, there may be errors or omissions in it and no legal responsibility is accepted for the information and opinions in this website
			</p>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>