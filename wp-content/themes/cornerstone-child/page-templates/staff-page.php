<?php
/**
 * Template Name: Staff page
 *
 * Description: The home page, no sidebar.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 2.3.2
 */

get_header(); ?>
<div class="row pad-double">
	
	<div id="content" class="large-12 columns" role="main">
		<div class="row pad-double">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>
				<?php comments_template( '', true ); ?>
			<?php endwhile; ?>
		</div>


		<div id="staff-filters" class="button-group filter-container">
		  <button class="tiny column triangle-isosceles custom-large-staff small-12 custom-small-staff custom-medium-staff staffFilter active" data-filter="*">all</button>
		  <button class="tiny column triangle-isosceles custom-large-staff small-12 custom-small-staff custom-medium-staff staffFilter" data-filter=".Management">management team</button>
		  <button class="tiny column triangle-isosceles custom-large-staff small-12 custom-small-staff custom-medium-staff staffFilter" data-filter=".IT">development team</button>
		  <button class="tiny column triangle-isosceles custom-large-staff small-12 custom-small-staff custom-medium-staff staffFilter" data-filter=".Sales">client managers</button>
		  <button class="tiny column triangle-isosceles custom-large-staff small-12 custom-small-staff custom-medium-staff staffFilter" data-filter=".Forecast">population forecasters</button>
		</div>
		<div class="row pad">
			<?php if(get_field('staff_member')): ?>
				<section class="staff-members">
					<?php 
						// $staff = get_field('staff_member');
						// browser()->log  ($staff );
					?>
					<?php while(has_sub_field('staff_member')): ?>
							<?php if(get_sub_field('featured')): ?>
								<article class="staff-member medium-6 large-4 columns <?php echo get_sub_field('category'); ?>">
									<div class="container drop-shadow clearfix">
										<i class="open-close-icn fa fa-plus-circle"></i>
										<div class="staff-summary clearfix">
											<img class="staff-photo" src="<?php $image = get_sub_field('staff_photo'); echo $image['url']; ?>" alt="">
											<div class="staff-info">
												<h1 class="ZurichBT-RomanCondensed staff-name"><?php the_sub_field('staff_name'); ?></h1>
												<blockquote>"<?php the_sub_field('staff_quote'); ?>"</blockquote>
											</div>
										</div>
										<div class="staff-role">
											<h3 class="staff-role-title"><?php the_sub_field('staff_title'); ?></h3>
											<p class="staff-role-description"><?php the_sub_field('staff_role_description'); ?></p>
										</div>
									</div>
								</article>
							<?php endif; ?>
					<?php endwhile ?>
				</section>
			<?php endif; ?>
		</div>
	</div>
	<?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>