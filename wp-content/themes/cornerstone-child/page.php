<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Cornerstone
 * @since Cornerstone 1.0
 */

get_header(); ?>
<div class="row pad-double">
	<div id="content" class="large-12 columns" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header class="hide entry-header">
					<h1 class="ZurichBT-RomanCondensed entry-title"><?php the_title(); ?></h1>
				</header>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>

				<footer class="entry-meta">
					<?php edit_post_link( __( 'Edit', 'cornerstone' ), '<span class="edit-link">', '</span>' ); ?>
				</footer>

			</article>

			<?php comments_template( '', true ); ?>

		<?php endwhile; ?>

	</div>

	
</div>
<!-- CONTENT BOXES -->
		<div class="row pad sub-heroes">
			<?php while(has_sub_field("sub_hero_box")): ?>
				<!-- Generic box -->
				<?php if(get_row_layout() == "generic_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
			 		<div class="sub-hero home-box medium-4 small-12 large-4 columns">
						<div class="generic <?php if (get_sub_field('is_page_link')) echo 'clickable' ?>" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div><?php the_sub_field("content"); ?></div>
							<?php if (get_sub_field('is_page_link')):?>
								<a href="<?php echo get_sub_field('page_link') ?>"></a>
							<?php endif; ?>
				 		</div>
					 </div>
				<!-- Blogroll box -->
				<?php elseif(get_row_layout() == "blogroll_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="blogroll" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="blog-feed"></div>
							<div>
								<h6 class="more-blog-posts"><a href="http://blog.id.com.au">see more blog posts</a></h6>
							</div>
				 		</div>
					 </div>
				<!-- Video box -->	 
				<?php elseif(get_row_layout() == "video_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="video" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="playVideo">
								<a href="<?php echo get_sub_field('youtube_video') ?>"><img class="image" src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt=""></a>
							</div>
				 		</div>
					 </div>
				<!-- Subscribe box -->
				<?php elseif(get_row_layout() == "subscribe_sub_hero_box" && get_sub_field('featured')): // layout: Content ?>
					<div class="sub-hero home-box medium-4 small-12 large-4 columns">
				 		<div class="subscribe" style="background-color:<?php echo get_sub_field('box_color')?>">
				 			<h1><?php the_sub_field("title"); ?></h1>
							<p><span class="p-arrow-white"></span><?php the_sub_field("subtitle"); ?></p>
							<div class="subscribe-form">
								<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>

								<script>
								  hbspt.forms.create({ 
								    portalId: '320463',
								    formId: '95441755-778f-41b8-b170-c4551c7b4396',
								    formData: {
							            cssClass: ''
							        },
							        validationOptions : {
							        	position : 'center left',
							        	messageClass : 'err'
							        },
							        onBeforeValidationInit:function(form){
							        	jQuery(form).validate();
							        }
								  });
								</script>
							</div>
				 		</div>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>

<?php get_footer(); ?>